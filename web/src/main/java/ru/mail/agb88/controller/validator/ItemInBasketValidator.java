package ru.mail.agb88.controller.validator;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.mail.agb88.service.dto.ItemDTO;

/**
 * Валидация изменения количества товара у пользователя
 */
@Service
public class ItemInBasketValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(ItemDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ItemDTO itemDTO = (ItemDTO) o;

        if (itemDTO.getQuantity() == null || itemDTO.getQuantity() < 1 || itemDTO.getQuantity() > 1000) {
            errors.rejectValue("quantity", "item.bad.quantity");
        }
    }
}
