package ru.mail.agb88.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.mail.agb88.service.NewsService;
import ru.mail.agb88.service.dto.NewsDTO;
import ru.mail.agb88.service.dto.NewsDTOWrapper;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by AlexBal.
 */
@Controller
public class NewsController {
    private final Logger logger = Logger.getLogger(NewsController.class);
    private NewsService newsService;
    private MessageSource msg;

    @Autowired
    public NewsController(NewsService newsService, @Qualifier("messageSource") MessageSource msg) {
        this.newsService = newsService;
        this.msg = msg;
    }

    // Выводит список новостей
    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String showNews(Model model, @RequestParam(required = false) Integer page) {
        logger.debug("News page");
        NewsDTOWrapper newsDTOWrapper = newsService.getAllNews(page);
        model.addAttribute("newsDTOWrapper", newsDTOWrapper);
        return "news/news";
    }

    // Подробнее о новости
    @RequestMapping(value = "/news/{newsId}", method = RequestMethod.GET)
    public String newsDetail(Model model, @PathVariable Long newsId) {
        logger.debug("Detail news page");
        NewsDTO newsDTO = newsService.getNews(newsId);
        model.addAttribute("newsDTO", newsDTO);
        return "news/newsdetail";
    }

    // Выводит форму добавления новости
    @RequestMapping(value = "/admin/news/addform", method = RequestMethod.GET)
    public String showNewsAddForm(Model model) {
        logger.debug("Form new news");
        model.addAttribute("newsDTO", new NewsDTO());
        return "news/addform";
    }

    // Выводит форму обновления новости
    @RequestMapping(value = "/admin/news/{newsId}", method = RequestMethod.GET, params = {"action=update"})
    public String showNewsUpdateForm(Model model, @PathVariable Long newsId, @RequestParam(required = false) boolean deleteImage) {
        logger.debug("Form update news");
        NewsDTO newsDTO = newsService.getNews(newsId);
        if (deleteImage) {
            newsDTO.setOriginalImageName(null);
        }
        model.addAttribute("newsDTO",newsDTO);
        return "news/updateform";
    }

    // Сохраняет новость
    @RequestMapping(value = "/admin/news", method = RequestMethod.POST)
    public String saveNews(Model model, @ModelAttribute @Valid NewsDTO newsDTO, BindingResult result) throws IOException {
        logger.debug("Adding new news");
        if (result.hasErrors()) {
            return "news/addform";
        }
        newsService.saveNewsDTO(newsDTO);
        model.addAttribute("result", msg.getMessage("news.success.add", null, Locale.getDefault()));
        showNews(model,1);
        return "news/news";
    }

    // Обновляет новость
    @RequestMapping(value = "/admin/news/{newsId}", method = RequestMethod.POST, params = {"action=update"})
    public String updateNews(Model model, @ModelAttribute @Valid NewsDTO newsDTO, BindingResult result, @PathVariable Long newsId) throws IOException {
        logger.debug("Updating new news");
        if (result.hasErrors()) {
            return "news/updateform";
        }
        newsDTO.setId(newsId);
        newsService.updateNewsDTO(newsDTO);
        model.addAttribute("result", msg.getMessage("news.success.update", null, Locale.getDefault()));
        showNews(model,1);
        return "news/news";
    }

    // Удаляет новость
    @RequestMapping(value = "/admin/news/{newsId}", method = RequestMethod.POST, params = {"action=delete"})
    public String deleteItem(Model model, @PathVariable Long newsId, @RequestParam String imageName) throws IOException {
        logger.debug("Deleting news");
        newsService.deleteNewsDTO(newsId, imageName);
        model.addAttribute("result", msg.getMessage("news.success.delete", null, Locale.getDefault()));
        showNews(model,1);
        return "news/news";
    }
}

