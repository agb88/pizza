package ru.mail.agb88.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.agb88.controller.validator.UserValidator;
import ru.mail.agb88.service.UserService;
import ru.mail.agb88.service.dto.UserDTO;

@Controller
public class RegisterController {

    private final Logger logger = Logger.getLogger(RegisterController.class);
    private UserValidator userValidator;
    private UserService userService;

    @Autowired
    public RegisterController(UserValidator userValidator, UserService userService) {
        this.userValidator = userValidator;
        this.userService = userService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegisterPage(Model model) {
        logger.debug("Register page");
        model.addAttribute("userDTO", new UserDTO());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@ModelAttribute UserDTO userDTO, BindingResult result) {
        userValidator.validate(userDTO, result);
        if (result.hasErrors()) {
            return "register";
        }
        userService.saveUserDTO(userDTO, true);
        return "redirect:/login";
    }
}
