package ru.mail.agb88.controller.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by AlexBal
 */
@Configuration
@ComponentScan("ru.mail.agb88")
public class AppConfig {
}
