package ru.mail.agb88.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.agb88.controller.config.security.LoginSuccessHandler;
import ru.mail.agb88.service.util.Util;
import ru.mail.agb88.service.dto.UserDTO;
import ru.mail.agb88.service.dto.UserPrincipal;

import java.util.Locale;

@Controller
public class GeneralController {

    private MessageSource msg;
    private LoginSuccessHandler loginSuccessHandler;

    @Autowired
    public GeneralController(LoginSuccessHandler loginSuccessHandler) {
        this.msg = msg;
        this.loginSuccessHandler = loginSuccessHandler;
    }

    // Страница логина
    @RequestMapping(value = {"/login", "/"}, method = RequestMethod.GET)
    public String login() {
        UserPrincipal userPrincipal = Util.findLoggedInUser();
        if (userPrincipal != null) {
            return "redirect:" + loginSuccessHandler.determineTargetUrl(userPrincipal);
        }
        return "login";
    }

    // Страница контактов
    @RequestMapping(value = "/contacts", method = RequestMethod.GET)
    public String showContacts(@ModelAttribute UserDTO userDTO) {
        return "user/contacts";
    }

    // Отправляет данные с форму отбратной связи из контактов
    @RequestMapping(value = "/contacts", method = RequestMethod.POST)
    public String getContacts(UserDTO userDTO, Model model) {
        // TODO Прием собщений от пользователей
        model.addAttribute("result", msg.getMessage("contacts.success", null, Locale.getDefault()));
        return "user/contacts";
    }
}
