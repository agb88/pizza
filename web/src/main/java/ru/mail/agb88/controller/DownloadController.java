package ru.mail.agb88.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;

@Controller
public class DownloadController {
    private final Logger logger = Logger.getLogger(DownloadController.class);
    private Environment env;

    @Autowired
    public DownloadController(Environment env) {
        this.env = env;
    }

    @RequestMapping(value = "/images/{imageType}/{imageName:.+}", method = RequestMethod.GET)
    public void downloadItems(HttpServletResponse resp, @PathVariable String imageType, @PathVariable String imageName) throws IOException {
        String filePath = env.getProperty("path_to_images") + imageType + "/" + imageName;

        File file = new File(filePath);
        if (imageName.equals("") || !file.exists()) { // Случай 1: Есть файл, нет ссылки в БД; случай 2: Нет файла, есть ссылка в БД
            file = new File(env.getProperty("path_to_images"), "noimage.jpg");
        }

        resp.setHeader("Content-Type", URLConnection.guessContentTypeFromName(file.getName()));
        resp.setHeader("Content-Length", String.valueOf(file.length()));
        resp.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");
        Files.copy(file.toPath(), resp.getOutputStream());
    }
}
