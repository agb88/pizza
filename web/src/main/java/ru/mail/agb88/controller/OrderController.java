package ru.mail.agb88.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.mail.agb88.repository.model.Status;
import ru.mail.agb88.service.OrderService;
import ru.mail.agb88.service.util.Util;
import ru.mail.agb88.service.dto.ItemsDTOWrapper;
import ru.mail.agb88.service.dto.UserDTO;

import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
public class OrderController {
    private final Logger logger = Logger.getLogger(OrderController.class);
    private final MessageSource msg;
    private UserItemController userItemController;
    private OrderService orderService;

    @Autowired
    public OrderController(UserItemController userItemController, OrderService orderService, MessageSource msg) {
        this.userItemController = userItemController;
        this.orderService = orderService;
        this.msg = msg;
    }

    // Выводит страницу с заказами для пользователя
    @RequestMapping(value = "/user/orders", method = RequestMethod.GET)
    public String showOrder(Model model) {
        logger.debug("Orders page for user");
        ItemsDTOWrapper itemsDTOWrapper = orderService.getAllOrdersForUser();
        model.addAttribute("itemsDTOWrapper", itemsDTOWrapper);
        return "user/orders";
    }

    // Добавление нового заказа
    @RequestMapping(value = "/user/orders", method = RequestMethod.POST)
    public String addOrder(Model model) {
        logger.debug("Adding order");
        int i = orderService.addOrder();
        String s = String.valueOf(i);
        model.addAttribute("result", msg.getMessage("order.success", new Object[]{s}, Locale.getDefault()));
        userItemController.showUserItems(model);
        return "user/items";
    }

    // Выводит страницу с заказами для админа
    @RequestMapping(value = "/admin/orders", method = RequestMethod.GET)
    public String showAllOrders(Model model) {
        logger.debug("Orders page for admin");
        Map<UserDTO, ItemsDTOWrapper> ordersMap = orderService.getAllOrders();
        model.addAttribute("ordersMap", ordersMap);
        List<Status> status = Util.getStatusList();
        model.addAttribute("status", status);
        return "admin/orders";
    }

    // Обновляет статус заказа
    @RequestMapping(value = "/admin/orders/{orderId}", method = RequestMethod.POST)
    public String showAllOrders(Model model, @PathVariable Long orderId, @RequestParam Status status) {
        logger.debug("Order changing status");
        orderService.changeStatus(orderId, status);
        model.addAttribute("result", msg.getMessage("changestatus.success", null, Locale.getDefault()));
        showAllOrders(model);
        return "admin/orders";
    }
}
