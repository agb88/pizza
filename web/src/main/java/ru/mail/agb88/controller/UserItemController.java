package ru.mail.agb88.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.mail.agb88.controller.validator.ItemValidator;
import ru.mail.agb88.service.ItemService;
import ru.mail.agb88.service.dto.ItemDTO;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserItemController {

    private final Logger logger = Logger.getLogger(UserItemController.class);
    private ItemService itemService;

    @Autowired
    public UserItemController(ItemService itemService, ItemValidator itemValidator, MessageSource msg) {
        this.itemService = itemService;
    }

    // Выводит список всех товаров для USER
    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public String showUserItems(Model model) {
        logger.debug("User catalog page");
        List<ItemDTO> itemsDTO = itemService.getAllItemsDTO();
        model.addAttribute("itemsDTO", itemsDTO);
        return "user/items";
    }
}
