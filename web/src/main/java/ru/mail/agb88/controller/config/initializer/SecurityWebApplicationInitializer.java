package ru.mail.agb88.controller.config.initializer;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * register the springSecurityFilter
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer{
}
