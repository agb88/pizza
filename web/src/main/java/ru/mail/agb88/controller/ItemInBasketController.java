package ru.mail.agb88.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.mail.agb88.controller.validator.ItemInBasketValidator;
import ru.mail.agb88.service.ItemInBasketService;
import ru.mail.agb88.service.dto.ItemDTO;
import ru.mail.agb88.service.dto.ItemsDTOWrapper;

import java.util.Locale;

@Controller
@RequestMapping("/user")
public class ItemInBasketController {

    private final Logger logger = Logger.getLogger(ItemInBasketController.class);
    private MessageSource msg;
    private ItemInBasketValidator itemInBasketValidator;
    private ItemInBasketService itemInBasketService;
    private UserItemController userItemController;

    @Autowired
    public ItemInBasketController(MessageSource msg, ItemInBasketValidator itemInBasketValidator, ItemInBasketService itemInBasketService, UserItemController userItemController) {
        this.msg = msg;
        this.itemInBasketValidator = itemInBasketValidator;
        this.itemInBasketService = itemInBasketService;
        this.userItemController = userItemController;
    }

    // Выводит список товаров в корзине
    @RequestMapping(value = "/basket", method = RequestMethod.GET)
    public String showItemsInBasket(Model model) {
        logger.debug("Show basket");
        ItemsDTOWrapper itemsDTOWrapper = itemInBasketService.findAllItemsDTOForUser();
        model.addAttribute("itemsDTOWrapper", itemsDTOWrapper);
        return "user/basket/basket";
    }

    // Добавляет товар в корзину пользователя
    @RequestMapping(value = "/basket/{itemId}", method = RequestMethod.POST)
    public String addItemInBasket(@PathVariable Long itemId, Model model) {
        logger.debug("Adding item in basket");
        itemInBasketService.addItemInBasket(itemId);
        model.addAttribute("result", msg.getMessage("iteminbasket.success.add", null, Locale.getDefault()));
        userItemController.showUserItems(model);
        return "user/items";
    }

    // Выводит форму для изменения количество товара в корзине
    @RequestMapping(value = "/basket/{itemId}", method = RequestMethod.GET, params = {"action=update"})
    public String showItemInBasketForm(@PathVariable Long itemId, Model model) {
        logger.debug("Show updating form");
        ItemDTO itemDTO = itemInBasketService.getItemInBasket(itemId);
        if (itemDTO == null) {
            return showItemsInBasket(model);
        }
        model.addAttribute("itemDTO", itemDTO);
        return "user/basket/basketform";
    }

    // Сохраняет новое количество товара в корзине у пользователя
    @RequestMapping(value = "/basket/{itemId}", method = RequestMethod.POST, params = {"action=update"})
    public String updateItemInBasket(@PathVariable Long itemId, Model model, @ModelAttribute ItemDTO itemDTO, BindingResult result) {
        logger.debug("Updating item in basket");
        itemInBasketValidator.validate(itemDTO, result);
        if (result.hasErrors()) {
            return "/user/basket/basketform";
        }
        itemDTO.setId(itemId);
        itemInBasketService.updateItemInBasket(itemDTO);
        showItemsInBasket(model);
        return "user/basket/basket";
    }

    // Сохраняет новое количество товара в корзине у пользователя
    @RequestMapping(value = "/basket/{itemId}", method = RequestMethod.POST, params = {"action=delete"})
    public String deleteItemInBasket(@PathVariable Long itemId, Model model) {
        logger.debug("Deleting item in basket");
        itemInBasketService.deleteItemInBasket(itemId);
        showItemsInBasket(model);
        return "user/basket/basket";
    }
}
