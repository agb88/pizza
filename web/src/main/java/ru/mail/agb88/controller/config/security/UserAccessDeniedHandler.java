package ru.mail.agb88.controller.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.stereotype.Component;
import ru.mail.agb88.service.util.Util;
import ru.mail.agb88.service.dto.UserPrincipal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Закрывает доступ зарегистрированным пользователям к страницам "/", "/login", "/register"
 */
@Component
public class UserAccessDeniedHandler extends AccessDeniedHandlerImpl{

    @Autowired
    private LoginSuccessHandler loginSuccessHandler;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        UserPrincipal userPrincipal = Util.findLoggedInUser();
        if (userPrincipal != null) {
            response.sendRedirect(loginSuccessHandler.determineTargetUrl(userPrincipal));
        } else {
            super.handle(request,response,accessDeniedException);
        }
    }
}
