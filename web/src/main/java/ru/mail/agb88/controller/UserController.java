package ru.mail.agb88.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.mail.agb88.service.UserService;
import ru.mail.agb88.service.dto.UserDTO;

@Controller
@RequestMapping(value = "/user")
public class UserController {
    private final Logger logger = Logger.getLogger(UserController.class);

    private MessageSource msg;
    private UserService userService;

    @Autowired
    public UserController(MessageSource msg, UserService userService) {
        this.msg = msg;
        this.userService = userService;
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public String showProfile(Model model, @PathVariable Long userId){
        logger.debug("Show user profile");
        UserDTO userDTO = userService.getUserDTO(userId);
        model.addAttribute("user", userDTO);
        return "user/profile/profile";
    }
}
