package ru.mail.agb88.controller.validator;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.mail.agb88.service.dto.ItemDTO;

/**
 * Проверяет поля формы добавления нового товара
 */
@Service
public class ItemValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(ItemDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ItemDTO itemDTO = (ItemDTO) o;

        try {
            Double price = Double.parseDouble(itemDTO.getPrice());
            if (price < 0) {
                errors.rejectValue("price", "price.negative");
            }
        } catch (NumberFormatException e) {
            errors.rejectValue("price", "price.not.number");
        }
    }
}

