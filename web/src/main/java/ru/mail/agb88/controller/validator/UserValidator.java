package ru.mail.agb88.controller.validator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.mail.agb88.service.UserService;
import ru.mail.agb88.service.dto.UserDTO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(UserDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDTO userDTO = (UserDTO) o;
        ValidationUtils.rejectIfEmpty(errors, "email", "email.empty");
        ValidationUtils.rejectIfEmpty(errors, "password", "password.empty");

        if (userDTO.getPassword().length() < 5) {
            errors.rejectValue("password", "password.length");
        }

        Pattern pattern = Pattern.compile("^\\w+@\\w+\\.\\w{2,3}$");
        Matcher matcher = pattern.matcher(userDTO.getEmail());

        if (!matcher.find()) {
            errors.rejectValue("email", "email.incorrect");
        }

        pattern = Pattern.compile("\\+\\d{12,14}");
        matcher = pattern.matcher(userDTO.getPhone());

        if (!matcher.find()) {
            errors.rejectValue("phone", "phone.format");
        }

        if (userService.getUserDTObyEmail(userDTO.getEmail()) != null) {
            errors.rejectValue("email", "email.exist");
        }
    }
}
