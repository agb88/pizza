package ru.mail.agb88.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.mail.agb88.repository.model.Role;
import ru.mail.agb88.service.UserService;
import ru.mail.agb88.service.dto.UserDTO;
import ru.mail.agb88.service.util.Util;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;


@Controller
@RequestMapping(value = "/admin")
public class AdminController {
    private final Logger logger = Logger.getLogger(AdminController.class);

    private MessageSource msg;
    private UserService userService;

    @Autowired
    public AdminController(MessageSource msg, UserService userService) {
        this.msg = msg;
        this.userService = userService;
    }

    // Показывает страницу со всеми пользователями
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String showUsers(Model model) {
        logger.debug("Show users for super admin");
        List<UserDTO> usersDTO = userService.getAllUsersDTO();
        model.addAttribute("usersDTO", usersDTO);
        List<Role> roles = Util.getRolesList();
        model.addAttribute("roles", roles);
        return "admin/users/users";
    }

    // Меняет роль пользователя
    @RequestMapping(value = "/users/{userId}", method = RequestMethod.POST, params = {"role"})
    public String showUser(Model model, @PathVariable Long userId, @RequestParam Role role) {
        logger.debug("User changing role");
        String message = userService.changeRole(userId, role);
        model.addAttribute("result", message);
        showUsers(model);
        return "admin/users/users";
    }

    // Блокирует пользователя
    @RequestMapping(value = "/users/{userId}", method = RequestMethod.POST, params = {"lock"})
    public String lockUser(Model model, @PathVariable Long userId, @RequestParam boolean lock) {
        logger.debug("User changing lock");
        String message = userService.lock(userId, lock);
        model.addAttribute("result", message);
        showUsers(model);
        return "admin/users/users";
    }

    // Выводит форму изменения пароля
    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET, params = {"action=update"})
    public String showPasswordForm(Model model, @PathVariable Long userId) {
        logger.debug("User password form");
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userId);
        model.addAttribute("userDTO", userDTO);
        return "admin/users/passwordform";
    }

    // Меняет пароль у пользователя из админки
    @RequestMapping(value = "/users", method = RequestMethod.GET, params = {"action=update"})
    public String changePassword(Model model, @ModelAttribute @Valid UserDTO userDTO, BindingResult result, @RequestParam Long userId) {
        logger.debug("Updating password from admin panel");
        if (result.hasErrors()) {
            return "admin/users/passwordform";
        }
        userDTO.setId(userId);
        userService.changePassword(userDTO);
        model.addAttribute("result", msg.getMessage("user.password.change", null, Locale.getDefault()));
        showUsers(model);
        return "admin/users/passwordform";
    }

    // Удаляет пользователя
    @RequestMapping(value = "/users/{userId}", method = RequestMethod.POST, params = {"action=delete"})
    public String lockUser(Model model, @PathVariable Long userId) {
        logger.debug("Deleting User");
        userService.delete(userId);
        model.addAttribute("result", msg.getMessage("user.delete", null, Locale.getDefault()));
        showUsers(model);
        return "admin/users/users";
    }
}
