package ru.mail.agb88.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.mail.agb88.controller.validator.ItemValidator;
import ru.mail.agb88.service.ItemService;
import ru.mail.agb88.service.dto.ItemDTO;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/admin")
public class ItemController {

    private final Logger logger = Logger.getLogger(ItemController.class);
    private ItemService itemService;
    private ItemValidator itemValidator;
    private MessageSource msg;

    @Autowired
    public ItemController(ItemService itemService, ItemValidator itemValidator, MessageSource msg) {
        this.itemService = itemService;
        this.itemValidator = itemValidator;
        this.msg = msg;
    }

    // Выводит список всех товаров для ADMIN и SUPERADMIN
    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public String showAdminItems(Model model) {
        logger.debug("Admin catalog page");
        List<ItemDTO> itemsDTO = itemService.getAllItemsDTO();
        model.addAttribute("itemsDTO", itemsDTO);
        return "admin/items/items";
    }

    // Выводит форму добавления нового товара
    @RequestMapping(value = "/items/addform", method = RequestMethod.GET)
    public String showItemAddForm(Model model) {
        logger.debug("Form new item");
        model.addAttribute("itemDTO", new ItemDTO());
        return "/admin/items/addform";
    }

    // Выводит форму для изменения или копирования товара
    @RequestMapping(value = "/items/{itemId}", method = RequestMethod.GET, params = {"action"})
    public String showUpdateCopyForm(Model model, @PathVariable Long itemId, @RequestParam String action, @RequestParam(required = false) boolean deleteImage) throws IOException {
        logger.debug("Form update/copy item");
        ItemDTO itemDTO = itemService.getItemDTOById(itemId);
        if (deleteImage) {
            itemDTO.setOriginalImageName(null);
        }
        model.addAttribute("itemDTO", itemDTO);
        return itemService.determineJspUrl(action);
    }

    // Сохраняет новый товар
    @RequestMapping(value = "/items", method = RequestMethod.POST)
    public String saveItem(Model model, @ModelAttribute ItemDTO itemDTO, BindingResult result) throws IOException {
        logger.debug("Adding new item");
        itemValidator.validate(itemDTO, result);
        if (result.hasErrors()) {
            return "/admin/items/addform";
        }
        itemService.saveItemDTO(itemDTO);
        model.addAttribute("result", msg.getMessage("item.success.add", null, Locale.getDefault()));
        showAdminItems(model);
        return "/admin/items/items";
    }

    // Дублирует товар
    @RequestMapping(value = "/items", method = RequestMethod.POST, params = {"action=copy"})
    public String copyItem(Model model, @ModelAttribute ItemDTO itemDTO, BindingResult result) throws IOException {
        logger.debug("Copying new item");
        itemValidator.validate(itemDTO, result);
        if (result.hasErrors()) {
            return "/admin/items/copyform";
        }
        itemService.copyItemDTO(itemDTO);
        model.addAttribute("result", msg.getMessage("item.success.duplicate", null, Locale.getDefault()));
        showAdminItems(model);
        return "/admin/items/items";
    }

    // Обновляет товар
    @RequestMapping(value = "/items/{itemId}", method = RequestMethod.POST, params = {"action=update"})
    public String updateItem(Model model, @ModelAttribute ItemDTO itemDTO, BindingResult result, @PathVariable Long itemId) throws IOException {
        logger.debug("Updating new item");
        itemValidator.validate(itemDTO, result);
        if (result.hasErrors()) {
            return "/admin/items/updateform";
        }
        itemDTO.setId(itemId);
        itemService.updateItemDTO(itemDTO);
        model.addAttribute("result", msg.getMessage("item.success.update", null, Locale.getDefault()));
        showAdminItems(model);
        return "/admin/items/items";
    }

    // Удаляет товар
    @RequestMapping(value = "/items/{itemId}", method = RequestMethod.POST, params = {"action=delete"})
    public String deleteItem(Model model, @PathVariable Long itemId, @RequestParam String imageName) throws IOException {
        logger.debug("Deleting item");
        itemService.deleteItemDTO(itemId, imageName);
        model.addAttribute("result", msg.getMessage("item.success.delete", null, Locale.getDefault()));
        showAdminItems(model);
        return "/admin/items/items";
    }
}
