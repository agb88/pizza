<%--Страница с новостями--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Каталог товаров</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery_3.2.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<sec:authorize access="hasAnyAuthority('ADMIN', 'SUPER_ADMIN')">
    <jsp:include page="/WEB-INF/pages/admin/adminnav.jsp">
        <jsp:param name="page" value="news"/>
    </jsp:include>
</sec:authorize>
<sec:authorize access="hasAnyAuthority('USER', 'ANONYMOUS')">
    <jsp:include page="/WEB-INF/pages/user/usernav.jsp">
        <jsp:param name="page" value="news"/>
    </jsp:include>
</sec:authorize>
<c:if test="${not empty result}">
    <div class="container">
        <div id="index-alert" class="alert alert-success alert-dismissable fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                <span class="glyphicon glyphicon-remove"/>
            </button>
            <p><h4><c:out value="${result}"/></h4></p>
        </div>
    </div>
</c:if>
<sec:authorize access="hasAnyAuthority('ADMIN', 'SUPER_ADMIN')">
    <div class="container">
        <a href="${pageContext.request.contextPath}/admin/news/addform" class="btn btn-default">Добавить новость</a>
    </div>
</sec:authorize>
<c:if test="${not empty newsDTOWrapper.newsDTO}">
    <div class="container">
        <div class="row masonry" data-columns>
            <c:forEach var="news" items="${newsDTOWrapper.newsDTO}">
                <div class="item">
                    <div class="caption">
                        <div class="row">
                            <div class="col-xs-9 col-xs-offset-2">
                                <br>
                                <img src="${pageContext.request.contextPath}/images/news/${news.imageName}"
                                     class="img-responsive center-block"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-9 col-xs-offset-2 text-center">
                                <a href="${pageContext.request.contextPath}/news/${news.id}"><h3><c:out
                                        value="${news.title}"/></h3></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-9 col-xs-offset-2 text-center">
                                <c:out value="Posted by ${news.userName} on ${news.creatingDate}"/>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-9 col-xs-offset-2">
                                <a href="${pageContext.request.contextPath}/news/${news.id}"><c:out
                                        value="${news.content}"/></a>
                            </div>
                        </div>
                        <br>
                        <sec:authorize access="hasAnyAuthority('ADMIN', 'SUPER_ADMIN')">
                            <div class="row">
                                <div class="col-xs-2 col-xs-offset-4">
                                    <form action="${pageContext.request.contextPath}/admin/news/${news.id}?action=update"
                                          method="GET">
                                        <input type="hidden" name="action" value="update"/>
                                        <input type="submit" class="btn btn-default pull-right" value="Изменить"/>
                                    </form>
                                </div>
                                <div class="col-xs-2 col-sm-offset-1">
                                    <form action="${pageContext.request.contextPath}/admin/news/${news.id}?action=delete"
                                          method="POST">
                                        <input type="hidden" name="imageName" value="${news.imageName}"/>
                                        <input type="submit" class="btn btn-default pull-right" value="Удалить"/>
                                    </form>
                                </div>
                            </div>
                        </sec:authorize>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
    <div class="navbar navbar-fixed-bottom">
        <div class="container">
            <div class="row">
                <ul class="pagination">
                    <li>
                        <a href="${pageContext.request.contextPath}/news?page=${newsDTOWrapper.currentPage-1}"><<</a>
                    </li>
                    <c:forEach step="1" begin="1" end="${newsDTOWrapper.pages}" var="page">
                        <li class="<c:if test="${page == newsDTOWrapper.currentPage}">active</c:if>">
                            <a href="${pageContext.request.contextPath}/news?page=${page}">
                                <c:out value="${page}"/>
                            </a>
                        </li>
                    </c:forEach>
                    <li>
                        <a href="${pageContext.request.contextPath}/news?page=${newsDTOWrapper.currentPage+1}">>></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</c:if>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/salvattore.min.js"></script>
</body>
</html>
