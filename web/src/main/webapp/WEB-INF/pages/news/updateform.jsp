<%--Форма изменения новости--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Добавление новости</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
</head>
<body>
<jsp:include page="/WEB-INF/pages/admin/adminnav.jsp">
    <jsp:param name="page" value="news"/>
</jsp:include>
<div class="container">
    <div class="row top-margin-addform">
        <div class="col-md-offset-2 col-md-8">
            <div class="well">
                <legend class="text-center">Добавление новости</legend>
                <form:form class="form-horizontal" modelAttribute="newsDTO"
                           action="${pageContext.request.contextPath}/admin/news/${newsDTO.id}?action=update"
                           method="POST" enctype="multipart/form-data" acceptCharset="UTF-8">
                    <p class="bg-danger">
                        <form:errors path="*" cssClass="bg-danger"/>
                    </p>
                    <div class="form-group">
                        <label for="title" class="col-md-3 control-label">Название</label>
                        <div class="col-md-9">
                            <form:input path="title" type="text" class="form-control" id="title" value="${newsDTO.title}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content" class="col-md-3 control-label">Содержание</label>
                        <div class="col-md-9">
                            <form:textarea path="content" class="form-control textarea-noresize" rows="10" id="content"
                                           value="${newsDTO.content}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="col-md-3 control-label">Картинка</label>
                        <div class="col-md-9">
                            <form:input path="image" type="file" name="image" id="image"/>
                        </div>
                    </div>
                    <c:if test="${newsDTO.originalImageName != null}">
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                <img src="${pageContext.request.contextPath}/images/news/${newsDTO.imageName}"
                                     class="img-responsive">
                            </div>
                            <div class="col-md-1">
                                <a class="btn btn-default close"
                                   href="${pageContext.request.contextPath}/admin/news/${newsDTO.id}?action=update&deleteImage=true">
                                    <span class="glyphicon glyphicon-remove"/></a>
                            </div>
                            <form:input path="originalImageName" type="hidden"
                                        value="${newsDTO.originalImageName}"/>
                        </div>
                    </c:if>
                    <div class="form-group">
                        <div class="input-group col-md-offset-6 col-md-4">
                            <a class="btn btn-default"
                               href="${pageContext.request.contextPath}/news">Отмена</a>
                            <input type="submit" class="btn btn-primary pull-right" value="Изменить"/>
                        </div>
                    </div>
                    <form:input path="imageName" type="hidden" value="${newsDTO.imageName}"/>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>