<%--Страница с подробной новостью--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Каталог товаров</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery_3.2.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<sec:authorize access="hasAnyAuthority('ADMIN', 'SUPER_ADMIN')">
    <jsp:include page="/WEB-INF/pages/admin/adminnav.jsp">
        <jsp:param name="page" value="news"/>
    </jsp:include>
</sec:authorize>
<sec:authorize access="hasAnyAuthority('USER', 'ANONYMOUS')">
    <jsp:include page="/WEB-INF/pages/user/usernav.jsp">
        <jsp:param name="page" value="news"/>
    </jsp:include>
</sec:authorize>
<div class="container">
    <a href="${pageContext.request.contextPath}/news" class="btn btn-default">Вернуться</a>
    <c:if test="${not empty newsDTO}">
        <div class="caption">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <img src="${pageContext.request.contextPath}/images/news/${newsDTO.imageName}"
                         class="img-responsive center-block"/>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2 text-center">
                    <h3><c:out value="${newsDTO.title}"/></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2 text-center">
                    <c:out value="Posted by ${newsDTO.userName} on ${newsDTO.creatingDate}"/>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <c:out value="${newsDTO.content}"/>
                </div>
            </div>
            <sec:authorize access="hasAnyAuthority('ADMIN', 'SUPER_ADMIN')">
                <br>
                <div class="row">
                    <div class="col-xs-1 col-xs-offset-5">
                        <form action="${pageContext.request.contextPath}/admin/news/${newsDTO.id}?action=update"
                              method="GET">
                            <input type="hidden" name="action" value="update"/>
                            <input type="submit" class="btn btn-default pull-right" value="Изменить"/>
                        </form>
                    </div>
                    <div class="col-xs-1">
                        <form action="${pageContext.request.contextPath}/admin/news/${newsDTO.id}?action=delete"
                              method="POST">
                            <input type="hidden" name="imageName" value="${newsDTO.imageName}"/>
                            <input type="submit" class="btn btn-default pull-right" value="Удалить"/>
                        </form>
                    </div>
                </div>
            </sec:authorize>
        </div>
    </c:if>
</div>
</body>
</html>
