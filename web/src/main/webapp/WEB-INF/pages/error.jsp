<%--Страница для ошибок--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Ошибка</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <div class="page-header">
                <h1>Ошибка</h1>
            </div>
            <br>
            <p>Возникла неизвестная ошибка в приложении. Пожалуйста, обратитесь к администратору.</p>
            <div class="col-md-offset-4 col-md-4">
                <a href="${pageContext.request.contextPath}/" class="btn btn-danger">На главную</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
