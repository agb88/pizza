<!-- Список всех товаров в корзине для user -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Корзина товаров</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery_3.2.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/pages/user/usernav.jsp">
    <jsp:param name="page" value="basket"/>
</jsp:include>
<c:choose>
    <c:when test="${not empty itemsDTOWrapper.itemsDTO}">
        <div class="container">
            <div class="text-center">
                <h1><c:out value="Список ваших заказов"/></h1>
            </div>
            <div class="row">
                <div class="col-xs-offset-2 col-xs-2">
                    <h3><c:out value="Название"/></h3>
                </div>
                <div class="col-xs-4">
                    <h3><c:out value="Описание"/></h3>
                </div>
                <div class="col-xs-1">
                    <h3><c:out value="Кол."/></h3>
                </div>
                <div class="col-xs-1">
                    <h3><c:out value="Цена"/></h3>
                </div>
                <div class="col-xs-2">
                    <form action="${pageContext.request.contextPath}/user/orders" method="POST"
                          style="margin-top: 15px">
                        <input type="submit" class="btn btn-danger btn-block" value="Отправить заказ"/>
                    </form>
                </div>
            </div>
            <hr/>
            <c:forEach var="item" items="${itemsDTOWrapper.itemsDTO}">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="">
                            <img src="${pageContext.request.contextPath}/images/items/${item.imageName}"
                                 class="img-responsive"/>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <h3><c:out value="${item.title}"/></h3>
                    </div>
                    <div class="col-xs-4" style="margin-top: 15px">
                        <c:out value="${item.description}"/>
                    </div>
                    <div class="col-xs-1 text-center">
                        <h3><c:out value="${item.quantity}"/></h3>
                    </div>
                    <div class="col-xs-1">
                        <h3><b><c:out value="${item.price}"/></b></h3>
                    </div>
                    <br/>
                    <div class="col-xs-1">
                        <form action="${pageContext.request.contextPath}/user/basket/${item.id}?action=update"
                              method="GET">
                            <input type="hidden" name="action" value="update"/>
                            <input type="submit" class="btn btn-default pull-right" value="Изменить"/>
                        </form>
                    </div>
                    <div class="col-xs-1">
                        <form action="${pageContext.request.contextPath}/user/basket/${item.id}?action=delete"
                              method="POST">
                            <input type="submit" class="btn btn-default pull-right" value="Удалить"/>
                        </form>
                    </div>
                </div>
                <hr/>
            </c:forEach>
            <div class="row">
                <div class="col-xs-offset-8 col-xs-2">
                    <h3><b><c:out value="Итого: ${itemsDTOWrapper.totalPrice}"/></b></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-offset-10 col-xs-2">
                    <form action="${pageContext.request.contextPath}/user/orders" method="POST">
                        <input type="submit" class="btn btn-danger btn-block" value="Отправить заказ"/>
                    </form>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="container">
            <div class="text-center">
                <h1><c:out value="Список заказов пуст"/></h1>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-offset-5 col-xs-2">
                    <a href="${pageContext.request.contextPath}/user/items"
                       class="btn btn-danger btn-block">В каталог</a>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>
</body>
</html>
