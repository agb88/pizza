<!-- Изменение количество товара -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Изменение количества</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
</head>
<body>
<jsp:include page="/WEB-INF/pages/user/usernav.jsp">
    <jsp:param name="page" value="basket"/>
</jsp:include>
<div class="container">
    <div class="row top-margin-addform">
        <div class="col-md-offset-4 col-md-4">
            <div class="row">
                <div class="well">
                    <legend>Изменение количества</legend>
                    <form:form class="form-horizontal" modelAttribute="itemDTO"
                               action="${pageContext.request.contextPath}/user/basket/${itemDTO.id}?action=update"
                               method="POST" acceptCharset="UTF-8">
                        <p class="bg-danger">
                            <form:errors path="*" cssClass="bg-danger"/>
                        </p>
                        <div class="form-group">
                            <label for="title" class="col-md-3 control-label">Название</label>
                            <div class="col-md-9">
                                <form:input path="title" disabled="true" type="text" class="form-control" id="title"
                                            value="${itemDTO.title}"/>
                                <form:input path="title" type="hidden" value="${itemDTO.title}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-md-3 control-label">Стоимость</label>
                            <div class="col-md-3">
                                <form:input path="price" disabled="true" type="text" class="form-control" id="price"
                                            value="${itemDTO.price}"/>
                                <form:input path="price" type="hidden" value="${itemDTO.price}"/>
                            </div>
                            <label for="quantity" class="col-md-3 control-label">Количество</label>
                            <div class="col-md-3">
                                <form:input path="quantity" type="text" class="form-control" id="quantity"
                                            value="${itemDTO.quantity}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="totalPrice" class="col-md-3 control-label">Итого</label>
                            <div class="col-md-9">
                                <form:input path="totalPrice" disabled="true" type="text" class="form-control"
                                            id="totalPrice" value="${itemDTO.totalPrice}"/>
                                <form:input path="totalPrice" type="hidden" value="${itemDTO.totalPrice}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-md-3 control-label">Описание</label>
                            <div class="col-md-9">
                                <form:textarea path="description" disabled="true" class="form-control textarea-noresize"
                                               rows="3" id="description" value="${itemDTO.description}"/>
                                <form:input path="description" type="hidden" value="${itemDTO.description}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2">
                                <img src="${pageContext.request.contextPath}/images/items/${itemDTO.imageName}"
                                     class="img-responsive">
                                <form:input path="imageName" type="hidden" value="${itemDTO.imageName}"/>
                                <form:input path="originalImageName" type="hidden"
                                            value="${itemDTO.originalImageName}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group col-md-offset-3 col-md-8">
                                <a class="btn btn-default"
                                   href="${pageContext.request.contextPath}/basket">Отмена</a>
                                <input type="submit" class="btn btn-primary pull-right" value="Изменить"/>
                            </div>
                        </div>
                        <form:input path="id" type="hidden" value="${itemDTO.id}"/>
                        <form:input path="invNumber" type="hidden" value="${itemDTO.invNumber}"/>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

