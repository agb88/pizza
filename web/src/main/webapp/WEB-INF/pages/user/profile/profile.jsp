<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--Профиль--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Профиль пользователя</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery_3.2.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<sec:authorize access="hasAnyAuthority('ADMIN', 'SUPER_ADMIN')">
    <sec:authentication property="principal.username" var="principal"/>
    <c:if test="${user.email eq principal}">
        <jsp:include page="/WEB-INF/pages/admin/adminnav.jsp">
            <jsp:param name="page" value="admin"/>
        </jsp:include>
    </c:if>
    <c:if test="${not user.email eq principal}">
        <jsp:include page="/WEB-INF/pages/admin/adminnav.jsp">
            <jsp:param name="page" value="users"/>
        </jsp:include>
    </c:if>

</sec:authorize>
<sec:authorize access="hasAnyAuthority('USER', 'ANONYMOUS')">
    <jsp:include page="/WEB-INF/pages/user/usernav.jsp">
        <jsp:param name="page" value="user"/>
    </jsp:include>
</sec:authorize>
<div class="container">
    <div class="page-header">
        <h2 align="center">Профиль пользователя <c:out value="${user.email}"/></h2>
    </div>
    <div class="row">
        <div class="col-md-4">
            <c:import url="userInfo.jsp"/>
        </div>
        <div class="col-md-8 text-center">
            Информация о заказах
        </div>
    </div>
</div>
</body>
</html>
