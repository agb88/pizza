<%--Блок данных о пользователе--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<form:form class="form-horizontal" modelAttribute="user"
           action="${pageContext.request.contextPath}/user/${user.id}" method="POST">
    <div class="well">
        <div class="col-md-offset-3 col-md-8 input-group">
            <p><h4><c:out value="Изменить профиль"/></h4></p>
            <small><c:out value="You must enter valid password"/></small>
        </div>
        <div class="hor-line">
        </div>
        <div class="form-group">
            <div class="col-md-3 text-right">
                <b><c:out value="email"/></b>
            </div>
            <div class="col-md-8">
                <c:out value="${user.email}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-md-3 control-label">password</label>
            <div class="input-group col-md-8">
                <form:errors path="password" cssClass="bg-danger"/>
                <form:input path="password" type="password" class="form-control" id="password"
                            placeholder="current password (min 5 symbols)" minlength="5"
                            required="required"/>
            </div>
        </div>
        <div class="form-group">
            <label for="newPassword" class="col-md-3 control-label" style="margin-top: -10px">New password</label>
            <div class="input-group col-md-8">
                <form:errors path="newPassword" cssClass="bg-danger"/>
                <form:input path="newPassword" type="password" class="form-control" id="newPassword"
                            placeholder="new password (min 5 symbols)" minlength="5" required="required"/>
            </div>
        </div>
        <div class="form-group">
            <label for="country" class="col-md-3 control-label">country</label>
            <div class="input-group col-md-8">
                <form:input path="country" type="text" class="form-control" name="country" id="country"
                            value="${user.country}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="city" class="col-md-3 control-label">city</label>
            <div class="input-group col-md-8">
                <form:input path="city" type="text" class="form-control" name="city" id="city"
                            value="${user.city}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="street" class="col-md-3 control-label">street</label>
            <div class="input-group col-md-8">
                <form:input path="street" type="text" class="form-control" name="street" id="street"
                            value="${user.street}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="home" class="col-md-3 control-label">home</label>
            <div class="input-group col-md-8">
                <form:input path="home" type="text" class="form-control" name="home" id="home"
                            value="${user.home}"/>
            </div>
        </div>
        <div class="form-group">
            <label for="app" class="col-md-3 control-label">app</label>
            <div class="input-group col-md-8">
                <form:input path="app" type="text" class="form-control" name="app" id="app"
                            value="${user.app}"/>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group col-md-offset-3 col-md-8">
                <a class="btn btn-default" href="${pageContext.request.contextPath}/">Отмена</a>
                <input type="submit" value="Изменить" class="btn btn-primary pull-right"/>
            </div>
        </div>
    </div>
</form:form>
