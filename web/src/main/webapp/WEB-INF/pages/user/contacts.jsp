<%--Форма обратной связи--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Контакты</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery_3.2.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/pages/user/usernav.jsp">
    <jsp:param name="page" value="contacts"/>
</jsp:include>
<div class="container">
    <div class="row">
        <c:if test="${not empty result}">
        <div class="col-xs-12">
            <div id="index-alert" class="alert alert-success alert-dismissable fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"/>
                </button>
                <p><h4><c:out value="${result}"/></h4></p>
            </div>
        </div>
    </div>
    </c:if>
    <form:form modelAttribute="userDTO" method="POST" action="${pageContext.request.contextPath}/contacts/">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_name">Firstname *</label>
                    <input disabled id="form_name" type="text" name="name" class="form-control"
                           placeholder="Please enter your firstname * (Not availible)" required="required"
                           data-error="Firstname is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_lastname">Lastname *</label>
                    <input disabled id="form_lastname" type="text" name="surname" class="form-control"
                           placeholder="Please enter your lastname * (Not availible)" required="required"
                           data-error="Lastname is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_email">Email *</label>
                    <form:input path="email" id="form_email" type="email" name="email" class="form-control"
                                placeholder="Please enter your email *" required="required"
                                data-error="Valid email is required."/>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_phone">Phone</label>
                    <input disabled id="form_phone" type="tel" name="phone" class="form-control"
                           placeholder="Please enter your phone (Not availible)">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="form_message">Message *</label>
                    <form:textarea path="description" id="form_message" class="form-control"
                                   placeholder="Message for me *" rows="4" required="required"
                                   data-error="Please,leave us a message."/>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Send message">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </form:form>
</div>
</body>
</html>
