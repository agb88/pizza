<%-- Страница с заказами пользователя--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Мои заказы</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery_3.2.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/pages/user/usernav.jsp">
    <jsp:param name="page" value="orders"/>
</jsp:include>
<c:choose>
    <c:when test="${not empty itemsDTOWrapper.itemsDTO}">
        <div class="container">
            <div class="text-center">
                <h1><c:out value="Список ваших заказов"/></h1>
            </div>
            <div class="row">
                <div class="col-xs-offset-3 col-xs-2">
                    <h3><c:out value="Название"/></h3>
                </div>
                <div class="col-xs-1">
                    <h3><c:out value="Дата"/></h3>
                </div>
                <div class="col-xs-2">
                    <h3><c:out value="Номер заказа"/></h3>
                </div>
                <div class="col-xs-1">
                    <h3><c:out value="Статус"/></h3>
                </div>
                <div class="col-xs-1">
                    <h3><c:out value="Кол."/></h3>
                </div>
                <div class="col-xs-1">
                    <h3><c:out value="Цена"/></h3>
                </div>
            </div>
            <hr/>
            <c:forEach var="item" items="${itemsDTOWrapper.itemsDTO}" varStatus="count">
                <div class="row">
                    <div class="col-xs-1">
                        <h3><c:out value="${count.index+1}"/></h3>
                    </div>
                    <div class="col-xs-2">
                        <div class="">
                            <img src="${pageContext.request.contextPath}/images/items/${item.imageName}"
                                 class="img-responsive"/>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <h3><c:out value="${item.title}"/></h3>
                    </div>
                    <div class="col-xs-1" style="margin-top: 20px">
                        <c:out value="${item.creatingDate}"/>
                    </div>
                    <div class="col-xs-2 text-center"  style="margin-top: 20px">
                        <c:out value="${item.orderId}"/>
                    </div>
                    <div class="col-xs-1 text-center" style="margin-top: 20px">
                        <c:out value="${item.status}"/>
                    </div>
                    <div class="col-xs-1">
                        <h3><c:out value="${item.quantity}"/></h3>
                    </div>
                    <div class="col-xs-1">
                        <h3><b><c:out value="${item.price}"/></b></h3>
                    </div>
                </div>
                <hr/>
            </c:forEach>
            <div class="row">
                <div class="col-xs-offset-9 col-xs-2">
                    <h3><b><c:out value="Итого: ${itemsDTOWrapper.totalPrice}"/></b></h3>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="container">
            <div class="text-center">
                <h1><c:out value="Список заказов пуст"/></h1>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-offset-5 col-xs-2">
                    <a href="${pageContext.request.contextPath}/user/items"
                       class="btn btn-danger btn-block">В каталог</a>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>
</body>
</html>