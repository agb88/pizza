<!-- Каталог user -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Каталог товаров</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery_3.2.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/pages/user/usernav.jsp">
    <jsp:param name="page" value="items"/>
</jsp:include>
<div class="container">
    <c:if test="${not empty result}">
        <div id="index-alert" class="alert alert-success alert-dismissable fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                <span class="glyphicon glyphicon-remove"/>
            </button>
            <p><h4><c:out value="${result}"/></h4></p>
        </div>
    </c:if>
    <c:if test="${not empty itemsDTO}">
        <div class="row">
            <c:forEach var="item" items="${itemsDTO}">
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <form action="${pageContext.request.contextPath}/user/basket/${item.id}" method="POST">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="center-block">
                                            <img src="${pageContext.request.contextPath}/images/items/${item.imageName}"
                                                 class="img-responsive"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3><c:out value="${item.title}"/></h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <c:out value="${item.description}"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3 col-xs-offset-1">
                                        <h3 class="red-text"><b><c:out value="${item.price}"/></b></h3>
                                    </div>
                                    <br/>
                                    <div class="col-xs-5 col-xs-offset-1">
                                        <input type="submit" class="btn btn-primary " value="Добавить в корзину"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </c:if>
</div>
</body>
</html>