<!-- Страница регистрации пользователя -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Регистрация</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <div class="page-header">
                <h1>Форма регистрации</h1>
            </div>
            <form:form class="form-horizontal" modelAttribute="userDTO"
                       action="${pageContext.request.contextPath}/register" method="POST">
                <div class="well">
                    <div class="input-group col-md-offset-3 col-md-8">
                        <span class="glyphicon glyphicon-asterisk"/><strong> Обязательное поле</strong>
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class="col-md-offset-3">
                            <form:errors path="*" cssClass="bg-danger"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">email</label>
                        <div class="input-group col-md-8">
                            <form:input path="email" type="email" class="form-control" name="email" id="email"
                                        placeholder="email"
                                        required="required"/>
                            <span class="input-group-addon glyphicon glyphicon-asterisk"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">password</label>
                        <div class="input-group col-md-8">
                            <form:input path="password" type="password" class="form-control" name="password"
                                        id="password"
                                        placeholder="password (min 5 symbols)" minlength="5" required="required"/>
                            <span class="input-group-addon glyphicon glyphicon-asterisk"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="country" class="col-md-3 control-label">country</label>
                        <div class="input-group col-md-8">
                            <form:input path="country" type="text" class="form-control" name="country" id="country"
                                        placeholder="Enter country"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-md-3 control-label">city</label>
                        <div class="input-group col-md-8">
                            <form:input path="city" type="text" class="form-control" name="city" id="city"
                                        placeholder="city"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="street" class="col-md-3 control-label">street</label>
                        <div class="input-group col-md-8">
                            <form:input path="street" type="text" class="form-control" name="street" id="street"
                                        placeholder="street"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="home" class="col-md-3 control-label">home</label>
                        <div class="input-group col-md-8">
                            <form:input path="home" type="text" class="form-control" name="home" id="home"
                                        placeholder="home"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="app" class="col-md-3 control-label">app</label>
                        <div class="input-group col-md-8">
                            <form:input path="app" type="text" class="form-control" name="app" id="app"
                                        placeholder="app"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group col-md-offset-3 col-md-8">
                            <a class="btn btn-default" href="${pageContext.request.contextPath}/">Отмена</a>
                            <input type="submit" value="Регистрация" class="btn btn-primary pull-right"/>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
