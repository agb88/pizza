<!-- Список всех товаров для admin -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Каталог для админа</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery_3.2.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/pages/admin/adminnav.jsp">
    <jsp:param name="page" value="items"/>
</jsp:include>
<div class="container">
    <div class="row">
        <div class="col-xs-2">
            <a class="btn btn-default" href="${pageContext.request.contextPath}/admin/items/addform">Добавить товар</a>
        </div>
        <c:if test="${not empty result}">
            <div class="col-xs-6">
                <div id="index-alert" class="alert alert-success alert-dismissable fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        <span class="glyphicon glyphicon-remove"/>
                    </button>
                    <p><h4><c:out value="${result}"/></h4></p>
                </div>
            </div>
        </c:if>
    </div>
</div>
<c:if test="${not empty itemsDTO}">
    <div class="container">
        <div class="text-center">
            <h1><c:out value="Список товаров"/></h1>
        </div>
        <div class="row">
            <div class="col-xs-offset-2 col-xs-2">
                <h3><c:out value="Название"/></h3>
            </div>
            <div class="col-xs-3">
                <h3><c:out value="Описание"/></h3>
            </div>
            <div class="col-xs-1">
                <h3><c:out value="Цена"/></h3>
            </div>
        </div>
        <hr/>
        <c:forEach var="item" items="${itemsDTO}">
            <div class="row">
                <div class="col-xs-2">
                    <div class="">
                        <img src="${pageContext.request.contextPath}/images/items/${item.imageName}"
                             class="img-responsive"/>
                    </div>
                </div>
                <div class="col-xs-2">
                    <h3><c:out value="${item.title}"/></h3>
                </div>
                <div class="col-xs-3" style="margin-top: 15px">
                    <c:out value="${item.description}"/>
                </div>
                <div class="col-xs-1">
                    <h3><b><c:out value="${item.price}"/></b></h3>
                </div>
                <br/>
                <div class="col-xs-4">
                    <div class="row">
                        <div class="col-xs-3 col-xs-offset-1">
                            <form action="${pageContext.request.contextPath}/admin/items/${item.id}?action=copy"
                                  method="GET">
                                <input type="hidden" name="action" value="copy"/>
                                <input type="submit" class="btn btn-default pull-right" value="Копировать"/>
                            </form>
                        </div>
                        <div class="col-xs-2 col-xs-offset-1">
                            <form action="${pageContext.request.contextPath}/admin/items/${item.id}?action=update"
                                  method="GET">
                                <input type="hidden" name="action" value="update"/>
                                <input type="submit" class="btn btn-default pull-right" value="Изменить"/>
                            </form>
                        </div>
                        <div class="col-xs-2 col-xs-offset-1">
                            <form action="${pageContext.request.contextPath}/admin/items/${item.id}?action=delete"
                                  method="POST">
                                <input type="hidden" name="imageName" value="${item.imageName}"/>
                                <input type="submit" class="btn btn-default pull-right" value="Удалить"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </c:forEach>
    </div>
</c:if>
</body>
</html>
