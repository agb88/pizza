<!-- Изменение товара -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Изменение товара</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
</head>
<body>
<jsp:include page="/WEB-INF/pages/admin/adminnav.jsp">
    <jsp:param name="page" value="items"/>
</jsp:include>
<div class="container">
    <div class="row top-margin-addform">
        <div class="col-md-offset-4 col-md-4">
            <div class="well">
                <legend>Изменение товара</legend>
                <form:form class="form-horizontal" modelAttribute="itemDTO"
                           action="${pageContext.request.contextPath}/admin/items/${itemDTO.id}?action=update"
                           method="POST" enctype="multipart/form-data" acceptCharset="UTF-8">
                    <p class="bg-danger">
                        <form:errors path="*" cssClass="bg-danger"/>
                    </p>
                    <div class="form-group">
                        <label for="title" class="col-md-3 control-label">Название</label>
                        <div class="col-md-9">
                            <form:input path="title" type="text" class="form-control" id="title"
                                        value="${itemDTO.title}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price" class="col-md-3 control-label">Стоимость</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <form:input path="price" type="text" class="form-control" id="price"
                                            value="${itemDTO.price}"/>
                                <span class="input-group-addon">Пример: 17.90</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="invNumber" class="col-md-3 control-label">Инв. номер</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <form:input path="invNumber" type="text" class="form-control" id="invNumber"
                                            value="${itemDTO.invNumber}"/>
                                <span class="input-group-addon">Пример: 18</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-md-3 control-label">Описание</label>
                        <div class="col-md-9">
                            <form:textarea path="description" class="form-control textarea-noresize" rows="3"
                                           id="description"
                                           value="${itemDTO.description}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="col-md-3 control-label">Картинка</label>
                        <div class="col-md-9">
                            <form:input path="image" type="file" name="image" id="image"/>
                        </div>
                    </div>
                    <c:if test="${itemDTO.originalImageName != null}">
                        <div class="form-group">
                            <div class="col-md-offset-1">
                                <a class="btn btn-default close"
                                   href="${pageContext.request.contextPath}/admin/items/${itemDTO.id}?action=update&deleteImage=true">
                                    <span class="glyphicon glyphicon-remove"/></a>
                                <img src="${pageContext.request.contextPath}/images/items/${itemDTO.imageName}"
                                     class="img-responsive">
                                <form:input path="originalImageName" type="hidden"
                                            value="${itemDTO.originalImageName}"/>
                            </div>
                        </div>
                    </c:if>
                    <div class="form-group">
                        <div class="input-group col-md-offset-3 col-md-8">
                            <a class="btn btn-default"
                               href="${pageContext.request.contextPath}/admin/items">Отмена</a>
                            <input type="submit" class="btn btn-primary pull-right" value="Обновить"/>
                        </div>
                    </div>
                    <form:input path="imageName" type="hidden" value="${itemDTO.imageName}"/>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
