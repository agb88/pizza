<!-- Навигационное меню admin -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:directive.page pageEncoding="UTF-8"/>
<div class="container">
    <div class="row">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#responsive-menu">
                        <span class="sr-only">Открыть навигацию</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Pizza</a>
                </div>
                <div class="collapse navbar-collapse" id="responsive-menu">
                    <ul class="nav navbar-nav">
                        <li class="<c:if test="${param.page eq 'news'}">active</c:if>"><a href="${pageContext.request.contextPath}/news">Новости</a></li>
                        <li class="<c:if test="${param.page eq 'items'}">active</c:if>"><a href="${pageContext.request.contextPath}/admin/items">Товары</a></li>
                        <li class="<c:if test="${param.page eq 'orders'}">active</c:if>"><a href="${pageContext.request.contextPath}/admin/orders">Заказы</a></li>
                        <sec:authorize access="hasAuthority('SUPER_ADMIN')"><li class="<c:if test="${param.page eq 'users'}">active</c:if>"><a href="${pageContext.request.contextPath}/admin/users">Пользователи</a></li></sec:authorize>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                    <sec:authorize access="isAuthenticated()"><li class="<c:if test="${param.page eq 'admin'}">active</c:if>"><a href="#"><sec:authentication property="principal.username"/></a></li></sec:authorize>
                        <li><a href="${pageContext.request.contextPath}/logout">Выход</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>


