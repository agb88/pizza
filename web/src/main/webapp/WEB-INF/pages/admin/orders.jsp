<%--Заказы пользователей--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Мои заказы</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery_3.2.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/pages/admin/adminnav.jsp">
    <jsp:param name="page" value="orders"/>
</jsp:include>
<c:choose>
    <c:when test="${not empty ordersMap}">
        <div class="container">
            <div class="text-center">
                <h1><c:out value="Список заказов"/></h1>
            </div>
            <div class="row">
                <div class="col-xs-2 col-xs-offset-2">
                    <h4><c:out value="Пользователь"/></h4>
                </div>
                <div class="col-xs-1">
                    <h4><c:out value="Название"/></h4>
                </div>
                <div class="col-xs-1">
                    <h4><c:out value="Дата"/></h4>
                </div>
                <div class="col-xs-1">
                    <h4><c:out value="Номер"/></h4>
                </div>
                <div class="col-xs-3 text-center">
                    <h4><c:out value="Статус"/></h4>
                </div>
                <div class="col-xs-1">
                    <h4><c:out value="Кол."/></h4>
                </div>
                <div class="col-xs-1">
                    <h4><c:out value="Итого"/></h4>
                </div>
            </div>
            <hr/>
            <c:forEach var="entry" items="${ordersMap}">
                <c:forEach var="item" items="${entry.value.itemsDTO}">
                    <div class="row">
                        <div class="col-xs-2">
                            <div class="">
                                <img src="${pageContext.request.contextPath}/images/items/${item.imageName}"
                                     class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-xs-2 text-center" style="margin-top: 20px">
                            <c:out value="${entry.key.email}"/>
                        </div>
                        <div class="col-xs-1" style="margin-top: 20px">
                            <c:out value="${item.title}"/>
                        </div>
                        <div class="col-xs-1" style="margin-top: 20px">
                            <c:out value="${item.creatingDate}"/>
                        </div>
                        <div class="col-xs-1 text-center" style="margin-top: 20px">
                            <c:out value="${item.orderId}"/>
                        </div>
                        <div class="col-xs-3 text-center" style="margin-top: 20px">
                            <form action="${pageContext.request.contextPath}/admin/orders/${item.orderId}"
                                  method="POST">
                                <div class="col-xs-8">
                                    <select name="status" class="form-control">
                                        <c:forEach var="status" items="${status}">
                                            <option
                                                    <c:if test="${status eq item.status}">selected</c:if>
                                                    value="${status}">
                                                <c:out value="${status}"/></option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="col-xs-4">
                                    <input class="btn btn-default" type="submit" value="Изменить">
                                </div>
                            </form>
                        </div>
                        <div class="col-xs-1 text-center">
                            <h3><c:out value="${item.quantity}"/></h3>
                        </div>
                        <div class="col-xs-1">
                            <h3><b><c:out value="${item.totalPrice}"/></b></h3>
                        </div>
                    </div>
                    <hr/>
                </c:forEach>
                <div class="row">
                    <div class="">
                        <b><c:out value="Сумма заказа пользователя ${entry.key.email}: ${entry.value.totalPrice}"/></b>
                        </h3>
                    </div>
                </div>
                <hr/>
            </c:forEach>
        </div>
    </c:when>
    <c:otherwise>
        <div class="container">
            <div class="text-center">
                <h1><c:out value="Список заказов пуст"/></h1>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-offset-5 col-xs-2">
                    <a href="${pageContext.request.contextPath}/admin/items"
                       class="btn btn-danger btn-block">К товарам</a>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>
</body>
</html>
