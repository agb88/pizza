<%--Страница изменения пароля из админки--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Изменение пароля пользователя</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <div class="page-header">
                <h1>Изменение пароля пользователя</h1>
            </div>
            <form:form class="form-horizontal" modelAttribute="userDTO"
                       action="${pageContext.request.contextPath}/admin/users" method="POST">
                <div class="well">
                    <div class="input-group col-md-offset-3 col-md-8">
                        <span class="glyphicon glyphicon-asterisk"/><strong> Обязательное поле</strong>
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class="col-md-offset-3">
                            <form:errors path="*" cssClass="bg-danger"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="oldpassword" class="col-md-3 control-label">старый пароль</label>
                        <div class="input-group col-md-8">
                            <form:input path="oldpassword" type="text" class="form-control" name="oldpassword"
                                        id="oldpassword"
                                        placeholder="oldpassword" required="required"/>
                            <span class="input-group-addon glyphicon glyphicon-asterisk"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">новый пароль</label>
                        <div class="input-group col-md-8">
                            <form:input path="password" type="password" class="form-control" name="password"
                                        id="password" placeholder="password (min 5 symbols)" minlength="5"
                                        required="required"/>
                            <span class="input-group-addon glyphicon glyphicon-asterisk"/>
                        </div>
                    </div>
                    <input type="hidden" name="userId" value="${userDTO.id}"/>
                    <div class="form-group">
                        <div class="input-group col-md-offset-3 col-md-8">
                            <a class="btn btn-default" href="${pageContext.request.contextPath}/admin/users">Отмена</a>
                            <input type="submit" value="Изменить" class="btn btn-primary pull-right"/>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>