<!-- Список всех пользователей для super_admin -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Список пользователей</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery_3.2.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/WEB-INF/pages/admin/adminnav.jsp">
    <jsp:param name="page" value="users"/>
</jsp:include>
<div class="container">
    <div class="row">
        <c:if test="${not empty result}">
            <div class="col-xs-6">
                <div id="index-alert" class="alert alert-success alert-dismissable fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        <span class="glyphicon glyphicon-remove"/>
                    </button>
                    <p><h4><c:out value="${result}"/></h4></p>
                </div>
            </div>
        </c:if>
    </div>
</div>
<c:if test="${not empty usersDTO}">
    <div class="container">
        <div class="text-center">
            <h1><c:out value="Список пользователей"/></h1>
        </div>
        <div class="row">
            <div class="col-xs-2 text-center">
                <h4><c:out value="Логин"/></h4>
            </div>
            <div class="col-xs-3 text-center">
                <h4><c:out value="Роль"/></h4>
            </div>
            <div class="col-xs-2 text-center">
                <h4><c:out value="Заблокирован"/></h4>
            </div>
        </div>
        <hr/>
        <c:forEach var="user" items="${usersDTO}" varStatus="count">
            <div class="row">
                <div class="col-xs-2">
                    <a href="${pageContext.request.contextPath}/user/${user.id}"><c:out value="${user.email}"/></a>
                </div>
                <div class="col-xs-3">
                    <form action="${pageContext.request.contextPath}/admin/users/${user.id}" method="POST">
                        <div class="col-xs-8">
                            <select name="role" class="form-control">
                                <c:forEach var="role" items="${roles}">
                                    <option
                                            <c:if test="${role eq user.role}">selected</c:if> value="${role}">
                                        <c:out value="${role}"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-xs-4">
                            <input class="btn btn-default" type="submit" value="Изменить">
                        </div>
                    </form>
                </div>
                <div class="col-xs-2">
                    <form action="${pageContext.request.contextPath}/admin/users/${user.id}" method="POST">
                        <div class="col-xs-8">
                            <select name="lock" class="form-control">
                                <option
                                        <c:if test="${user.locked eq true}">selected</c:if> value="${true}">
                                    <c:out value="да"/>
                                </option>
                                <option
                                        <c:if test="${user.locked eq false}">selected</c:if> value="${false}">
                                    <c:out value="нет"/>
                                </option>
                            </select>
                        </div>
                        <div class="col-xs-4">
                            <c:if test='${user.locked eq true}'><c:set var="buttonName" value="Разблокировать"/></c:if>
                            <c:if test='${user.locked eq false}'><c:set var="buttonName" value="Заблокировать"/></c:if>
                            <input class="btn btn-default" type="submit" value="${buttonName}">
                        </div>
                    </form>
                </div>
                <div class="col-xs-4 col-xs-offset-1">
                    <div class="col-xs-6">
                        <form action="${pageContext.request.contextPath}/admin/users/${user.id}?action=update"
                              method="GET">
                            <input type="hidden" name="action" value="update"/>
                            <input type="submit" class="btn btn-default" value="Изменить пароль"/>
                        </form>
                    </div>
                    <div class="col-xs-6">
                        <form action="${pageContext.request.contextPath}/admin/users/${user.id}"
                              method="POST">
                            <input type="hidden" name="action" value="delete"/>
                            <input type="submit" class="btn btn-default" value="Удалить"/>
                        </form>
                    </div>
                </div>
            </div>
            <hr>
        </c:forEach>
    </div>
</c:if>
</body>
</html>
