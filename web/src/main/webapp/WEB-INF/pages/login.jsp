<!-- Ввод логина и пароля -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Вход</title>
    <link href="<c:url value='${pageContext.request.contextPath}/resources/css/bootstrap.css'/>" type="text/css"
          rel="stylesheet"/>
    <link href="<c:url value='${pageContext.request.contextPath}/resources/css/style.css'/>" type="text/css"
          rel="stylesheet"/>
</head>
<body><%--
<c:if test="${not empty userDTO}">
    <c:redirect url="/checklogin"/>
</c:if>--%>
<div class="container">
    <div class="row top-margin-login">
        <div class="col-md-offset-4 col-md-4">
            <div class="well">
                <legend>Авторизация на сайте</legend>
                <form class="form-horizontal" action="<c:url value='/login'/>" method="POST">
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <c:if test="${param.containsKey('error')}">
                                <div class="bg-danger">Username or password is not valid or user blocked</div>
                            </c:if>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-2 control-label">Email</label>
                        <div class="col-md-10">
                            <input type="text" id="email" class="form-control" placeholder="email" name="email"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" id="password" class="col-md-2 control-label">Пароль</label>
                        <div class="col-md-10">
                            <input type="password" class="form-control" placeholder="пароль" name="password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <input type="submit" class="btn btn-block form-control btn-primary" value="Войти"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <a class="btn btn-block form-control btn-default"
                               href="${pageContext.request.contextPath}/register">Зарегистрироваться</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

