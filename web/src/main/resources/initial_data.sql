CREATE DATABASE IF NOT EXISTS pizza;
USE pizza;

DROP TABLE IF EXISTS PIZZA.T_NEWS;
DROP TABLE IF EXISTS PIZZA.T_ORDERS;
DROP TABLE IF EXISTS PIZZA.T_BASKETS;
DROP TABLE IF EXISTS PIZZA.T_ITEMS;
DROP TABLE IF EXISTS PIZZA.T_USERS;

CREATE TABLE PIZZA.T_NEWS(
  F_ID BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  F_USER_ID BIGINT(20),
  F_TITLE VARCHAR(50),
  F_CONTENT VARCHAR(10000),
  F_IMAGE VARCHAR(255),
  F_ORIGINAL_IMAGE VARCHAR(255),
  F_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT USER_NEWS FOREIGN KEY (F_USER_ID) REFERENCES T_USERS (F_ID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE PIZZA.T_ORDERS(
  F_ID BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  F_USER_ID BIGINT(20),
  F_ITEM_ID BIGINT(20),
  F_QUANTITY INT(5),
  F_STATUS INT(5),
  F_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT USER_ORDERS FOREIGN KEY (F_USER_ID) REFERENCES T_USERS (F_ID) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT ITEM_ORDERS FOREIGN KEY (F_ITEM_ID) REFERENCES T_ITEMS (F_ID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE PIZZA.T_BASKETS(
  F_ID BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  F_USER_ID BIGINT(20),
  F_ITEM_ID BIGINT(20),
  F_QUANTITY INT(5),
  CONSTRAINT USER_BASKETS FOREIGN KEY (F_USER_ID) REFERENCES T_USERS (F_ID) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT ITEM_BASKETS FOREIGN KEY (F_ITEM_ID) REFERENCES T_ITEMS (F_ID) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE PIZZA.T_ITEMS(
  F_ID BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  F_TITLE VARCHAR(50),
  F_PRICE INT(11),
  F_INV_NUMBER VARCHAR(10),
  F_DESCRIPTION VARCHAR(255),
  F_IMAGE VARCHAR(255),
  F_ORIGINAL_IMAGE VARCHAR(255)
);

CREATE TABLE PIZZA.T_USERS(
  F_ID BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  F_EMAIL VARCHAR(50),
  F_PASSWORD VARCHAR(100),
  F_COUNTRY VARCHAR(50),
  F_CITY VARCHAR(50),
  F_STREET VARCHAR(50),
  F_HOME VARCHAR(5),
  F_APP VARCHAR(5),
  F_ROLE INT(5) DEFAULT '2' NOT NULL,
  F_LOCKED TINYINT(1) DEFAULT '0',
  F_PHONE VARCHAR(255)
);

INSERT INTO PIZZA.T_NEWS (F_USER_ID, F_TITLE, F_CONTENT, F_IMAGE, F_ORIGINAL_IMAGE, F_DATE) VALUES (3,	'Заголовок',	'Самым быстрым способом передачи информации от одного человека к другому является печатное слово. В зависимости от поставленных задач и целевой аудитории, набор выразительных средств русского языка может значительно изменяться. И читающему, и пишущему человеку важно знать, как определить стиль текста, ведь это даст понимание написанного, а еще позволит очертить целый ряд возможных приемов, с помощью которых до читателя легко донести мысли.',	'2017-08-28 13:20:08',	'news.jpg',	'news.jpg');
INSERT INTO PIZZA.T_NEWS (F_USER_ID, F_TITLE, F_CONTENT, F_IMAGE, F_ORIGINAL_IMAGE, F_DATE) VALUES (2,	'Собака',	'Собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака',	'2017-08-29 20:53:16',	'dog.jpg',	'dog_29_08_2017_17_57_40.jpg');
INSERT INTO PIZZA.T_NEWS (F_USER_ID, F_TITLE, F_CONTENT, F_IMAGE, F_ORIGINAL_IMAGE, F_DATE) VALUES (2,	'Собака',	'Собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака',	'2017-08-29 20:53:16',	'dog.jpg',	'dog_29_08_2017_17_57_40.jpg');
INSERT INTO PIZZA.T_NEWS (F_USER_ID, F_TITLE, F_CONTENT, F_IMAGE, F_ORIGINAL_IMAGE, F_DATE) VALUES (2,	'Собака',	'Собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака',	'2017-08-29 20:53:16',	'dog.jpg',	'dog_29_08_2017_17_57_40.jpg');
INSERT INTO PIZZA.T_NEWS (F_USER_ID, F_TITLE, F_CONTENT, F_IMAGE, F_ORIGINAL_IMAGE, F_DATE) VALUES (2,	'Собака',	'Собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака',	'2017-08-29 20:53:16',	'dog.jpg',	'dog_29_08_2017_17_57_40.jpg');
INSERT INTO PIZZA.T_NEWS (F_USER_ID, F_TITLE, F_CONTENT, F_IMAGE, F_ORIGINAL_IMAGE, F_DATE) VALUES (2,	'Собака',	'Собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака',	'2017-08-29 20:53:16',	'dog.jpg',	'dog_29_08_2017_17_57_40.jpg');
INSERT INTO PIZZA.T_NEWS (F_USER_ID, F_TITLE, F_CONTENT, F_IMAGE, F_ORIGINAL_IMAGE, F_DATE) VALUES (2,	'Собака',	'Собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака собака',	'2017-08-29 20:53:16',	'dog.jpg',	'dog_29_08_2017_17_57_40.jpg');

INSERT INTO PIZZA.T_ORDERS (F_USER_ID, F_ITEM_ID, F_QUANTITY, F_STATUS, F_DATE) VALUES (3, 1, 10, '2017-09-01 09:48:41', 3);
INSERT INTO PIZZA.T_ORDERS (F_USER_ID, F_ITEM_ID, F_QUANTITY, F_DATE, F_STATUS) VALUES (3, 2, 4, '2017-08-29 20:37:04', 2);
INSERT INTO PIZZA.T_ORDERS (F_USER_ID, F_ITEM_ID, F_QUANTITY, F_DATE, F_STATUS) VALUES (3, 3, 2, '2017-08-29 20:37:04', 3);
INSERT INTO PIZZA.T_ORDERS (F_USER_ID, F_ITEM_ID, F_QUANTITY, F_DATE, F_STATUS) VALUES (3, 6, 1, '2017-08-29 20:37:04', 3);

INSERT INTO PIZZA.T_ITEMS (F_TITLE, F_PRICE, F_INV_NUMBER, F_DESCRIPTION, F_IMAGE, F_ORIGINAL_IMAGE) VALUES ('Биф Тейсти', 1501, '1', 'Вкусная биф тейсти пицца', 'biftasty.jpg', 'biftasty.jpg');
INSERT INTO PIZZA.T_ITEMS (F_TITLE, F_PRICE, F_INV_NUMBER, F_DESCRIPTION, F_IMAGE, F_ORIGINAL_IMAGE) VALUES ('Четыре сыра', 1400, '2', 'Вкусная пицца четыре сыра', 'fourcheese.jpg', 'fourcheese.jpg');
INSERT INTO PIZZA.T_ITEMS (F_TITLE, F_PRICE, F_INV_NUMBER, F_DESCRIPTION, F_IMAGE, F_ORIGINAL_IMAGE) VALUES ('Итальяно', 1700, '3', 'Оригинальная итальянская пицца', 'italiano.jpg', 'italiano.jpg');
INSERT INTO PIZZA.T_ITEMS (F_TITLE, F_PRICE, F_INV_NUMBER, F_DESCRIPTION, F_IMAGE, F_ORIGINAL_IMAGE) VALUES ('Украинская', 1790, '4', 'Замечательная пицца с украинскими мотивами', 'ukrainian.jpg', 'ukrainian.jpg');
INSERT INTO PIZZA.T_ITEMS (F_TITLE, F_PRICE, F_INV_NUMBER, F_DESCRIPTION, F_IMAGE, F_ORIGINAL_IMAGE) VALUES ('Баварская', 1799, '5', 'Острая пицца', 'bavarskaya_06_08_2017_01_08_47.jpg', 'bavarskaya.jpg');

INSERT INTO PIZZA.T_USERS (F_EMAIL, F_PASSWORD, F_LOCKED, F_ROLE, F_PHONE, F_COUNTRY, F_CITY, F_STREET, F_HOME, F_APP) VALUES ('super', '$2a$04$S8HJtxayd8p6n0c1alKgpeEvwIgMFQ7KG9hyUYp/cL8is5jn0Hn/e', 0, 0, '+375172100000', null, null, null, null, null);
INSERT INTO PIZZA.T_USERS (F_EMAIL, F_PASSWORD, F_LOCKED, F_ROLE, F_PHONE, F_COUNTRY, F_CITY, F_STREET, F_HOME, F_APP) VALUES ('admin', '$2a$04$S8HJtxayd8p6n0c1alKgpeEvwIgMFQ7KG9hyUYp/cL8is5jn0Hn/e', 0, 0, '+375172100000', null, null, null, null, null);
INSERT INTO PIZZA.T_USERS (F_EMAIL, F_PASSWORD, F_LOCKED, F_ROLE, F_PHONE, F_COUNTRY, F_CITY, F_STREET, F_HOME, F_APP) VALUES ('user', '$2a$04$S8HJtxayd8p6n0c1alKgpeEvwIgMFQ7KG9hyUYp/cL8is5jn0Hn/e', 0, 2, '+375172100000', null, null, null, null, null);