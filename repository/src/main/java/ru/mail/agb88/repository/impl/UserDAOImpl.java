package ru.mail.agb88.repository.impl;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.agb88.repository.UserDAO;
import ru.mail.agb88.repository.model.Role;
import ru.mail.agb88.repository.model.User;

import javax.persistence.NoResultException;

@Repository
public class UserDAOImpl extends GenericDAOImpl<User, Long> implements UserDAO {
    private Logger logger = Logger.getLogger(UserDAOImpl.class);

    @Override
    public User findByEmail(String email) {
        Query query = getSession().createQuery("SELECT u from User u where u.email = :email")
                .setParameter("email", email);
        try {
            return (User) query.getSingleResult();
        } catch (NoResultException nre) {
            logger.error("Ошибка findByEmail: нет такого пользователя");
            // Ignoring no result exception to get null
        }
        return null;
    }

    @Override
    public void changeStatus(Long userId, Role role) {
        Query query = getSession().createQuery("UPDATE User u SET u.role = :role WHERE u.id = :userId")
                .setParameter("role", role)
                .setParameter("userId", userId);
        query.executeUpdate();
    }

    @Override
    public int countSuperAdmin() {
        Query query = getSession().createQuery("SELECT count (u.role) FROM User u WHERE u.role = :role")
                .setParameter("role", Role.SUPER_ADMIN);
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public void lock(Long userId) {
        Query query = getSession().createQuery("UPDATE User u SET u.locked = true WHERE u.id = :userId")
                .setParameter("userId", userId);
        query.executeUpdate();
    }

    @Override
    public void unLock(Long userId) {
        Query query = getSession().createQuery("UPDATE User u SET u.locked = false WHERE u.id = :userId")
                .setParameter("userId", userId);
        query.executeUpdate();
    }

    @Override
    public void delete(Long userId) {
        Query query = getSession().createQuery("DELETE FROM User u WHERE u.id = :userId")
                .setParameter("userId", userId);
        query.executeUpdate();
    }
}
