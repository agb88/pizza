package ru.mail.agb88.repository.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class Address {

    @Column(name = "F_COUNTRY")
    private String country;

    @Column (name = "F_CITY")
    private String city;

    @Column (name = "F_STREET")
    private String street;

    @Column (name = "F_HOME")
    private String home;

    @Column (name = "F_APP")
    private String app;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(country, address.country) &&
                Objects.equals(city, address.city) &&
                Objects.equals(street, address.street) &&
                Objects.equals(home, address.home) &&
                Objects.equals(app, address.app);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, city, street, home, app);
    }
}
