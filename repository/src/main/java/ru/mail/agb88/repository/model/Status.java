package ru.mail.agb88.repository.model;

/**
 Cтатус заказа
 */
public enum Status {
    NEW ("new"),
    REVIEWING("reviewing"),
    IN_PROGRESS ("in progress"),
    DELIVERED ("delivered");

    Status(String name) {
        this.name = name;
    }

    String name;

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
