package ru.mail.agb88.repository.impl;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.agb88.repository.ItemInBasketDAO;
import ru.mail.agb88.repository.model.ItemInBasket;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class ItemInBasketDAOImpl extends GenericDAOImpl<ItemInBasket, Long> implements ItemInBasketDAO {
    private Logger logger = Logger.getLogger(ItemInBasketDAOImpl.class);

    @Override
    public List<ItemInBasket> findAllForUser(Long userId) {
        TypedQuery<ItemInBasket> query = getSession().createQuery("SELECT i FROM ItemInBasket i JOIN FETCH i.user u JOIN FETCH i.item WHERE u.id = :userId");
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Override
    public int deleteAllForUser(Long userId) {
        Query query = getSession().createQuery("DELETE FROM ItemInBasket i WHERE i.user.id = :userId").setParameter("userId", userId);
        return query.executeUpdate();
    }

    // получаем товар из корзины или null
    @Override
    public ItemInBasket findByUserIdAndItemId(Long userId, Long itemId) {
        ItemInBasket itemInBasket = null;
        Query query = getSession().createQuery("SELECT i FROM ItemInBasket i WHERE i.user.id = :userId AND i.item.id = :itemId");
        query.setParameter("userId", userId);
        query.setParameter("itemId", itemId);
        try {
            itemInBasket = (ItemInBasket) query.getSingleResult();
        } catch (NoResultException nre) {
            logger.error("Ошибка findByUserIdAndItemId: нет такого товара с id = " + itemId + " в корзине у пользователя " + userId);
            // Ignoring no result exception to get null
        }
        return itemInBasket;
    }

    // сохраняет товар SQL запросом по id пользователя и товара (без получения Item)
    @Override
    public void save(Long userId, Long itemId) {
        Query query = getSession().createNativeQuery("INSERT INTO T_BASKETS (F_USER_ID, F_ITEM_ID, F_QUANTITY) VALUES (?,?,?)");
        query.setParameter(1, userId);
        query.setParameter(2, itemId);
        query.setParameter(3, 1);
        query.executeUpdate();
    }
}
