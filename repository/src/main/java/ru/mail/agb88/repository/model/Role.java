package ru.mail.agb88.repository.model;

/**
 * enum для ролей
 */
public enum Role {
    SUPER_ADMIN("super admin"),
    ADMIN("admin"),
    USER("user");

    Role(String name) {
        this.name = name;
    }

    String name;

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}