package ru.mail.agb88.repository.model;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "T_ORDERS")
public class Order implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Long id;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "F_USER_ID")
    @Fetch(FetchMode.JOIN)
    private User user;

    @ManyToOne
    @JoinColumn(name = "F_ITEM_ID")
    @Fetch(FetchMode.JOIN)
    private Item item;

    @Column(name = "F_QUANTITY")
    private Integer quantity;

    @Temporal(TemporalType.TIMESTAMP)
    //@org.hibernate.annotations.CreationTimestamp // создает сам дату добавления
    @org.hibernate.annotations.UpdateTimestamp
    @Column(name = "F_DATE")
    private Date creatingDate;

    @Enumerated(value = EnumType.ORDINAL) // Запись цифрами
    @Column(name = "F_STATUS")
    private Status status;

    public Order() {
    }

    public Order(User user, Item item, int quantity, Status status) {
        this.user = user;
        this.item = item;
        this.quantity = quantity;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return quantity.equals(order.quantity) &&
                Objects.equals(user, order.user) &&
                Objects.equals(item, order.item) &&
                Objects.equals(creatingDate, order.creatingDate) &&
                status == order.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, item, quantity, creatingDate, status);
    }
}
