package ru.mail.agb88.repository.impl;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.agb88.repository.NewsDAO;
import ru.mail.agb88.repository.model.News;
import ru.mail.agb88.repository.model.Order;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class NewsDAOImpl extends GenericDAOImpl<News, Long> implements NewsDAO{
    Logger logger = Logger.getLogger(NewsDAOImpl.class);

    @Override
    public List<News> findAll(Integer startPosition, Integer maxResults) {
        TypedQuery<News> news = getSession().createQuery("SELECT n FROM News n JOIN FETCH n.user ORDER BY n.creatingDate DESC ");
        news.setFirstResult(startPosition);
        news.setMaxResults(maxResults);
        return news.getResultList();
    }

    @Override
    public Integer countNews() {
        Integer countNews = null;
        Query query = getSession().createQuery("SELECT count(n.id) FROM News n");
        try{
            countNews = ((Long) query.getSingleResult()).intValue();
        } catch (NoResultException nre){
            logger.error("Ошибка countNews: новостей нет");
            // Ignoring no result exception to get null
        }
        return countNews;
    }

    @Override
    public void delete(Long newsId) {
        Query query = getSession().createQuery("DELETE FROM News n WHERE n.id = :newsId")
                .setParameter("newsId", newsId);
        query.executeUpdate();
    }
}
