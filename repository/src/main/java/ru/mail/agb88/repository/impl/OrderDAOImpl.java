package ru.mail.agb88.repository.impl;

import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.mail.agb88.repository.OrderDAO;
import ru.mail.agb88.repository.UserDAO;
import ru.mail.agb88.repository.model.Order;
import ru.mail.agb88.repository.model.Status;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class OrderDAOImpl extends GenericDAOImpl<Order, Long> implements OrderDAO {
    private final Logger logger = Logger.getLogger(OrderDAOImpl.class);
    private UserDAO userDAO;

    @Autowired
    public OrderDAOImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public List<Order> getAllOrdersForUser(Long userId) {
        TypedQuery<Order> query = getSession().createQuery("SELECT o FROM Order o JOIN FETCH o.item WHERE o.user.id = :userId");
        query.setParameter("userId", userId);
        return query.getResultList();

    }

    @Override
    public Order findByUserIdAndItemId(Long userId, Long itemId) {
        Order order = null;
        Query query = getSession().createQuery("SELECT o FROM Order o WHERE o.user.id = :userId AND o.item.id = :itemId")
                .setParameter("userId", userId)
                .setParameter("itemId", itemId);
        try {
            order = (Order) query.getSingleResult();
        } catch (NoResultException nre) {
            logger.error("Ошибка findByUserIdAndItemId: нет такого заказа");
            // Ignoring no result exception to get null
        }
        return order;
    }

    @Override
    public void changeStatus(Long orderId, Status status) {
        Query query = getSession().createQuery("UPDATE Order o SET o.status = :status WHERE o.id = :orderId")
                .setParameter("status", status)
                .setParameter("orderId", orderId);
        query.executeUpdate();
    }
}
