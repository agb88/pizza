package ru.mail.agb88.repository.impl;


import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ru.mail.agb88.repository.ItemDAO;
import ru.mail.agb88.repository.model.Item;

@Repository
public class ItemDAOImpl extends GenericDAOImpl<Item, Long> implements ItemDAO {

    @Override
    public void delete(Long itemId) {
        Query query = getSession().createQuery("DELETE FROM Item i WHERE i.id = :itemId").setParameter("itemId", itemId);
        query.executeUpdate();
    }
}
