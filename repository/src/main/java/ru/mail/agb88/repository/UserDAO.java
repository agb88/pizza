package ru.mail.agb88.repository;

import ru.mail.agb88.repository.model.Role;
import ru.mail.agb88.repository.model.User;


public interface UserDAO extends GenericDAO<User, Long> {
    User findByEmail (String email);

    void changeStatus(Long userId, Role role);

    int countSuperAdmin();

    void lock(Long userId);

    void unLock(Long userId);

    void delete(Long userId);
}
