package ru.mail.agb88.repository;

import ru.mail.agb88.repository.model.Item;


public interface ItemDAO extends GenericDAO<Item, Long> {
    void delete(Long itemId);
}
