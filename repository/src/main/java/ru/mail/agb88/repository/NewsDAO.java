package ru.mail.agb88.repository;

import ru.mail.agb88.repository.model.News;

import java.util.List;


public interface NewsDAO extends GenericDAO<News, Long> {
    List<News> findAll(Integer startPosition, Integer maxResults);

    Integer countNews();

    void delete(Long newsId);
}
