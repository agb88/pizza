package ru.mail.agb88.repository;


import ru.mail.agb88.repository.model.Order;
import ru.mail.agb88.repository.model.Status;

import java.util.List;


public interface OrderDAO extends GenericDAO<Order, Long> {
    List<Order> getAllOrdersForUser(Long userId);

    Order findByUserIdAndItemId(Long userId, Long itemId);

    void changeStatus(Long orderId, Status status);
}
