package ru.mail.agb88.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "T_NEWS")
public class News implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "F_USER_ID")
    private User user;

    @Column(name = "F_TITLE", nullable = false)
    private String title;

    @Column(name = "F_CONTENT")
    private String content;

    @Temporal(TemporalType.TIMESTAMP)
    @org.hibernate.annotations.CreationTimestamp
    @Column(name = "F_DATE", updatable = false)
    private Date creatingDate;

    @Column(name = "F_IMAGE")
    private String imageName;

    @Column(name = "F_ORIGINAL_IMAGE")
    private String originalImageName;

    public News() {
    }

    public News(Long id, User user, String title, String content, String imageName, String originalImageName) {
        this.id = id;
        this.user = user;
        this.title = title;
        this.content = content;
        this.imageName = imageName;
        this.originalImageName = originalImageName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Date creatingDate) {
        this.creatingDate = creatingDate;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getOriginalImageName() {
        return originalImageName;
    }

    public void setOriginalImageName(String originalImageName) {
        this.originalImageName = originalImageName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return Objects.equals(title, news.title) &&
                Objects.equals(content, news.content) &&
                Objects.equals(imageName, news.imageName) &&
                Objects.equals(originalImageName, news.originalImageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, content, imageName, originalImageName);
    }
}
