package ru.mail.agb88.repository.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table (name = "T_USERS")
public class User implements Serializable {
    @EmbeddedId
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "F_ID", insertable = false, updatable = false)
    private Long id;

    @Column (name = "F_EMAIL", nullable = false)
    private String email;

    @Column (name = "F_PASSWORD")
    private String password;

    @Column (name = "F_ROLE")
    @Enumerated(value = EnumType.ORDINAL)
    private Role role;

    @Column (name = "F_LOCKED")
    private boolean locked;

    @Column(name = "F_PHONE")
    private String phone;

    @Embedded
    private Address address;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<News> news = new HashSet<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<ItemInBasket> itemsInBasket = new HashSet<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Order> orders = new HashSet<>();

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    public Set<ItemInBasket> getItemsInBasket() {
        return itemsInBasket;
    }

    public void setItemsInBasket(Set<ItemInBasket> itemsInBasket) {
        this.itemsInBasket = itemsInBasket;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id) &&
                Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email);
    }
}