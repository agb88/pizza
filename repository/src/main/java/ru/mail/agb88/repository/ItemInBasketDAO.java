package ru.mail.agb88.repository;

import ru.mail.agb88.repository.model.ItemInBasket;

import java.util.List;


public interface ItemInBasketDAO extends GenericDAO<ItemInBasket, Long> {
    ItemInBasket findByUserIdAndItemId(Long userId, Long itemId);

    void save(Long userId, Long itemId);

    List<ItemInBasket> findAllForUser(Long userId);

    int deleteAllForUser(Long userId);
}
