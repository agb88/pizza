package ru.mail.agb88.service.dto;


import ru.mail.agb88.repository.model.ItemInBasket;
import ru.mail.agb88.repository.model.News;
import ru.mail.agb88.repository.model.Order;
import ru.mail.agb88.repository.model.Role;

import javax.validation.constraints.Min;
import java.util.Objects;
import java.util.Set;

public class UserDTO {

    private Long id;
    private String email;
    private String description;
    @Min(5)
    private String password;
    private String newPassword;
    private Role role;
    private boolean locked;
    private String phone;
    private String country;
    private String city;
    private String street;
    private String home;
    private String app;
    private Set<News> news;
    private Set<ItemInBasket> itemInBasket;
    private Set<Order> orders;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String oldpassword) {
        this.newPassword = newPassword;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    public Set<ItemInBasket> getItemInBasket() {
        return itemInBasket;
    }

    public void setItemInBasket(Set<ItemInBasket> itemInBasket) {
        this.itemInBasket = itemInBasket;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public boolean hasAddress() {
        return (!getCountry().equals("")) || (!getCity().equals("")) || (!getStreet().equals("")) || (!getHome().equals("")) || (!getApp().equals(""));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(id, userDTO.id) &&
                Objects.equals(email, userDTO.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email);
    }

    public static final class Builder {
        private Long id;
        private String email;
        private String description;
        private String password;
        private Role role;
        private boolean blocked;
        private String phone;
        private Set<News> news;
        private Set<ItemInBasket> itemsInBasket;
        private Set<Order> orders;

        private Builder() {
        }

        public static Builder newUserDTO() {
            return new Builder();
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder role(Role role) {
            this.role = role;
            return this;
        }

        public Builder blocked(boolean blocked) {
            this.blocked = blocked;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder news(Set<News> news) {
            this.news = news;
            return this;
        }

        public Builder itemsInBasket(Set<ItemInBasket> itemsInBasket) {
            this.itemsInBasket = itemsInBasket;
            return this;
        }

        public Builder orders(Set<Order> orders) {
            this.orders = orders;
            return this;
        }

        public UserDTO build() {
            UserDTO userDTO = new UserDTO();
            userDTO.id = this.id;
            userDTO.email = this.email;
            userDTO.password = this.password;
            userDTO.role = this.role;
            userDTO.locked = this.blocked;
            userDTO.phone = this.phone;
            userDTO.itemInBasket = this.itemsInBasket;
            userDTO.orders = this.orders;
            userDTO.news = this.news;
            userDTO.description = this.description;
            return userDTO;
        }
    }
}
