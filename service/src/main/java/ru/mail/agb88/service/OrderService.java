package ru.mail.agb88.service;


import ru.mail.agb88.repository.model.Status;
import ru.mail.agb88.service.dto.ItemsDTOWrapper;
import ru.mail.agb88.service.dto.UserDTO;

import java.util.Map;

public interface OrderService {
    int addOrder();

    ItemsDTOWrapper getAllOrdersForUser();

    Map<UserDTO, ItemsDTOWrapper> getAllOrders();

    void changeStatus(Long orderId, Status status);
}
