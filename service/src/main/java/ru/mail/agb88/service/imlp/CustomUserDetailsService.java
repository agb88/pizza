package ru.mail.agb88.service.imlp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.mail.agb88.repository.model.User;
import ru.mail.agb88.service.UserService;
import ru.mail.agb88.service.dto.UserPrincipal;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    private final Logger logger = Logger.getLogger(CustomUserDetailsService.class);

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userService.getUserByEmail(email);
        if (user == null) {
            logger.info("User not found");
            throw new UsernameNotFoundException("Username not found");
        }
        return new UserPrincipal(user);
    }
}
