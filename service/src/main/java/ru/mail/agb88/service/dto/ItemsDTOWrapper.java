package ru.mail.agb88.service.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * Обертка для List<ItemDTO>
 */
public class ItemsDTOWrapper {
    private List<ItemDTO> itemsDTO;
    private String totalPrice;
    private BigDecimal countTotalPrice = new BigDecimal(0);

    public void countTotalPrice(BigDecimal countTotalPrice) {
        this.countTotalPrice = this.countTotalPrice.add(countTotalPrice);
    }

    public List<ItemDTO> getItemsDTO() {
        return itemsDTO;
    }

    public void setItemsDTO(List<ItemDTO> itemsDTO) {
        totalPrice = countTotalPrice.toString();
        this.itemsDTO = itemsDTO;
    }

    public String getTotalPrice() {
        totalPrice = countTotalPrice.toString();
        return totalPrice;
    }
}
