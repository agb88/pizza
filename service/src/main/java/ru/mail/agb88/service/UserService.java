package ru.mail.agb88.service;


import ru.mail.agb88.repository.model.Role;
import ru.mail.agb88.repository.model.User;
import ru.mail.agb88.service.dto.UserDTO;

import javax.validation.Valid;
import java.util.List;

public interface UserService {
    UserDTO getUserDTO(Long id);

    UserDTO getUserDTObyEmail(String email);

    List<UserDTO> getAllUsersDTO();

    void saveUserDTO(UserDTO userDTO);

    void saveUserDTO(UserDTO userDTO, boolean authenticate);

    User getUserByEmail(String email);

    void deleteUserDTO(UserDTO userDTO);

    String changeRole(Long userId, Role role);

    String lock(Long userId, boolean lock);

    void delete(Long userId);

    void changePassword(@Valid UserDTO userDTO);
}
