package ru.mail.agb88.service.dto;


import org.springframework.web.multipart.MultipartFile;
import javax.validation.constraints.NotNull;

public class NewsDTO {
    private Long id;
    private Long userId;
    private String userName;
    @NotNull
    private String title;
    @NotNull
    private String content;
    private String imageName;
    private String originalImageName;
    private MultipartFile image;
    private String creatingDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getOriginalImageName() {
        return originalImageName;
    }

    public void setOriginalImageName(String originalImageName) {
        this.originalImageName = originalImageName;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public String getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(String creatingDate) {
        this.creatingDate = creatingDate;
    }

    public static final class NewsDTOBuilder {
        private Long id;
        private Long userId;
        private String userName;
        private String title;
        private String content;
        private String imageName;
        private String originalImageName;
        private MultipartFile image;
        private String creatingDate;

        private NewsDTOBuilder() {
        }

        public static NewsDTOBuilder newNewsDTO() {
            return new NewsDTOBuilder();
        }

        public NewsDTOBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public NewsDTOBuilder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public NewsDTOBuilder userName(String userName) {
            this.userName = userName;
            return this;
        }

        public NewsDTOBuilder title(String title) {
            this.title = title;
            return this;
        }

        public NewsDTOBuilder content(String content) {
            this.content = content;
            return this;
        }

        public NewsDTOBuilder imageName(String imageName) {
            this.imageName = imageName;
            return this;
        }

        public NewsDTOBuilder originalImageName(String originalImageName) {
            this.originalImageName = originalImageName;
            return this;
        }

        public NewsDTOBuilder image(MultipartFile image) {
            this.image = image;
            return this;
        }

        public NewsDTOBuilder creatingDate(String creatingDate) {
            this.creatingDate = creatingDate;
            return this;
        }

        public NewsDTO build() {
            NewsDTO newsDTO = new NewsDTO();
            newsDTO.setId(id);
            newsDTO.setUserId(userId);
            newsDTO.setUserName(userName);
            newsDTO.setTitle(title);
            newsDTO.setContent(content);
            newsDTO.setImageName(imageName);
            newsDTO.setOriginalImageName(originalImageName);
            newsDTO.setImage(image);
            newsDTO.setCreatingDate(creatingDate);
            return newsDTO;
        }
    }
}
