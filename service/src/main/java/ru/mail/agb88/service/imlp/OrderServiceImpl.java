package ru.mail.agb88.service.imlp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.agb88.repository.ItemInBasketDAO;
import ru.mail.agb88.repository.OrderDAO;
import ru.mail.agb88.repository.model.ItemInBasket;
import ru.mail.agb88.repository.model.Order;
import ru.mail.agb88.repository.model.Status;
import ru.mail.agb88.service.OrderService;
import ru.mail.agb88.service.util.Converter;
import ru.mail.agb88.service.util.Util;
import ru.mail.agb88.service.dto.ItemDTO;
import ru.mail.agb88.service.dto.ItemsDTOWrapper;
import ru.mail.agb88.service.dto.UserDTO;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {

    private final Logger logger = Logger.getLogger(OrderServiceImpl.class);
    private OrderDAO orderDAO;
    private ItemInBasketDAO itemInBasketDAO;

    @Autowired
    public OrderServiceImpl(OrderDAO orderDAO, ItemInBasketDAO itemInBasketDAO) {
        this.orderDAO = orderDAO;
        this.itemInBasketDAO = itemInBasketDAO;
    }

    // Переносит из корзины в заказы, удаляет корзину пользователя.
    // Если в заказах уже есть такой заказ, то увеличивает его количество.
    // Возвращает число перенесенных строк.
    @Override
    @Transactional
    public int addOrder() {
        List<ItemInBasket> itemsInBasket = itemInBasketDAO.findAllForUser(Util.findLoggedInUser().getId());
        List<Order> orders = itemsInBasketToOrders(itemsInBasket);
        for (Order order : orders) {
            Order oldOrder = orderDAO.findByUserIdAndItemId(Util.findLoggedInUser().getId(), order.getItem().getId());
            if (oldOrder != null) {
                oldOrder.setQuantity(oldOrder.getQuantity() + order.getQuantity());
            } else {
                orderDAO.save(order);
            }
        }
        return itemInBasketDAO.deleteAllForUser(Util.findLoggedInUser().getId());
    }

    // Возвращает все заказы пользователя
    @Override
    @Transactional
    public ItemsDTOWrapper getAllOrdersForUser() {
        //TODO Почему идет доп запрос на пользователя?
        List<Order> orders = orderDAO.getAllOrdersForUser(Util.findLoggedInUser().getId());
        if (orders.isEmpty()) {
            return null;
        }
        ItemsDTOWrapper itemsDTOWrapper = new ItemsDTOWrapper();
        List<ItemDTO> itemsDTO = new ArrayList<>();
        for (Order order : orders) {
            itemsDTO.add(orderToItemDTO(order, itemsDTOWrapper));
        }
        itemsDTOWrapper.setItemsDTO(itemsDTO);
        return itemsDTOWrapper;
    }

    // Возвращает карту со всеми заказами. Ключ - пользователь, значение - заказы пользователя в обертке.
    @Override
    @Transactional
    public Map<UserDTO, ItemsDTOWrapper> getAllOrders() {
        List<Order> orders = orderDAO.findAll();
        if (orders.isEmpty()) {
            return null;
        }
        Map<UserDTO, ItemsDTOWrapper> ordersMap = new HashMap<>();
        for (Order order : orders) {
            UserDTO userDTO = Converter.userToUserDTO(order.getUser());
            ItemsDTOWrapper tempItemsDTOWrapper = ordersMap.get(userDTO);
            if (tempItemsDTOWrapper != null) {
                ItemDTO itemDTO = orderToItemDTO(order, tempItemsDTOWrapper);
                tempItemsDTOWrapper.getItemsDTO().add(itemDTO);
            } else {
                tempItemsDTOWrapper = new ItemsDTOWrapper();
                List<ItemDTO> itemsDTO = new ArrayList<>();
                ItemDTO itemDTO = orderToItemDTO(order, tempItemsDTOWrapper);
                itemsDTO.add(itemDTO);
                tempItemsDTOWrapper.setItemsDTO(itemsDTO);
                ordersMap.put(userDTO, tempItemsDTOWrapper);
            }
        }
        return ordersMap;
    }

    // Меняет статус у товара в заказе
    @Override
    @Transactional
    public void changeStatus(Long orderId, Status status) {
        orderDAO.changeStatus(orderId, status);
    }

    private ItemDTO orderToItemDTO(Order order, ItemsDTOWrapper itemsDTOWrapper) {
        BigDecimal price = new BigDecimal(order.getItem().getPrice()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
        Integer quantity = order.getQuantity();
        BigDecimal totalPrice = price.multiply(new BigDecimal(quantity));
        if (itemsDTOWrapper != null) {
            itemsDTOWrapper.countTotalPrice(totalPrice);
        }
        String creatingDate = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy").format(order.getCreatingDate());
        return ItemDTO.Builder.newItemDTO()
                .id(order.getItem().getId())
                .title(order.getItem().getTitle())
                .price(price.toString())
                .totalPrice(totalPrice.toString())
                .invNumber(order.getItem().getInvNumber())
                .description(order.getItem().getDescription())
                .imageName(order.getItem().getImageName())
                .originalImageName(order.getItem().getOriginalImageName())
                .quantity(quantity)
                .orderId(order.getId())
                .creatingDate(creatingDate)
                .status(order.getStatus())
                .build();
    }


    private List<Order> itemsInBasketToOrders(List<ItemInBasket> itemInBaskets) {
        List<Order> orders = new ArrayList<>();
        for (ItemInBasket itemInBasket : itemInBaskets) {
            orders.add(new Order(itemInBasket.getUser(), itemInBasket.getItem(), itemInBasket.getQuantity(), Status.NEW));
        }
        return orders;
    }
}
