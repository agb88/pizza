package ru.mail.agb88.service.imlp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import ru.mail.agb88.repository.NewsDAO;
import ru.mail.agb88.repository.model.News;
import ru.mail.agb88.repository.model.User;
import ru.mail.agb88.service.NewsService;
import ru.mail.agb88.service.util.Util;
import ru.mail.agb88.service.dto.NewsDTO;
import ru.mail.agb88.service.dto.NewsDTOWrapper;
import ru.mail.agb88.service.dto.UserPrincipal;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AlexBal.
 */
@Service
public class NewsServiceImpl implements NewsService {
    private Logger logger = Logger.getLogger(NewsServiceImpl.class);

    private NewsDAO newsDAO;
    private Environment env;

    @Autowired
    public NewsServiceImpl(NewsDAO newsDAO, Environment env) {
        this.newsDAO = newsDAO;
        this.env = env;
    }

    // Вовзращает список всех новостей
    @Override
    @Transactional
    public NewsDTOWrapper getAllNews(Integer currentPage) {
        if (currentPage == null || currentPage <= 0) {
            currentPage = 1;
        }

        Integer maxNews = Integer.parseInt(env.getProperty("max_news"));
        Integer countNews = newsDAO.countNews();
        Integer pages = getPages(countNews, maxNews);
        if (currentPage > pages) {
            currentPage = pages;
        }

        Integer startPosition = currentPage * maxNews - maxNews;
        List<News> news = newsDAO.findAll(startPosition, maxNews);
        List<NewsDTO> newsDTO = new ArrayList<>();
        for (News oneNews : news) {
            NewsDTO newsDTOTemp = newsToNewsDTO(oneNews);
            newsDTO.add(trimContent(newsDTOTemp));
        }
        return new NewsDTOWrapper(newsDTO, countNews, currentPage, pages);
    }

    // Получает конкретную новость
    @Override
    @Transactional
    public NewsDTO getNews(Long newsId) {
        News news = newsDAO.findById(newsId);
        return newsToNewsDTO(news);
    }

    // Сохраняет новость
    @Override
    @Transactional
    public void saveNewsDTO(NewsDTO newsDTO) throws IOException {
        String modifiedImageName = null;
        if (!newsDTO.getImage().isEmpty()) {
            modifiedImageName = Util.getModifiedImageName(newsDTO.getImage().getOriginalFilename());
            newsDTO.setOriginalImageName(newsDTO.getImage().getOriginalFilename());
            newsDTO.setImageName(modifiedImageName);
        }

        newsDAO.save(newsDTOtoNews(newsDTO));

        if (!newsDTO.getImage().isEmpty()) {
            File result = new File(env.getProperty("path_to_images") + "news/" + modifiedImageName);
            FileCopyUtils.copy(newsDTO.getImage().getBytes(), result);
        }
    }

    // Обновляет новость
    @Override
    @Transactional
    public void updateNewsDTO(NewsDTO newsDTO) throws IOException {
        String oldImageName = newsDTO.getImageName();
        if (!newsDTO.getImage().isEmpty()) {
            saveNewsDTO(newsDTO);
            new File(env.getProperty("path_to_images") + "news/" + oldImageName).delete();
            return;
        }

        if (newsDTO.getOriginalImageName() == null) {
            new File(env.getProperty("path_to_images") + "news/" + oldImageName).delete();
            newsDTO.setImageName(null);
        }

        newsDAO.save(newsDTOtoNews(newsDTO));
    }

    // Удаляет новость
    @Override
    @Transactional
    public void deleteNewsDTO(Long newsId, String imageName) throws IOException {
        newsDAO.delete(newsId);
        File file = new File(env.getProperty("path_to_images") + "news/" + imageName);
        if (file.exists()) {
            try {
                file.delete();
            } catch (SecurityException e) {
                logger.debug("File not found or can't delete");
            }
        }
    }

    // Обрезает длинную новость до 80 символов
    private NewsDTO trimContent(NewsDTO newsDTOTemp) {
        String content = newsDTOTemp.getContent();
        if (content.length() > 80) {
            newsDTOTemp.setContent(newsDTOTemp.getContent().substring(0, 80) + "...");
        }
        return newsDTOTemp;
    }

    // Возвращает количество страниц для пагинации
    // countNews - количество новостей в БД
    // maxNews - количество выводимых на страницу новостей
    private Integer getPages(Integer countNews, Integer maxNews) {
        Integer pages;
        if (countNews % maxNews == 0) {
            pages = countNews / maxNews;
        } else {
            pages = countNews / maxNews + 1;
        }
        return pages;
    }

    private News newsDTOtoNews(NewsDTO newsDTO) {
        UserPrincipal loggedInUser = Util.findLoggedInUser();
        User user = new User();
        user.setId(loggedInUser.getId());
        News news = new News(newsDTO.getId()
                , user
                , newsDTO.getTitle()
                , newsDTO.getContent()
                , newsDTO.getImageName()
                , newsDTO.getOriginalImageName());
        return news;
    }

    private NewsDTO newsToNewsDTO(News oneNews) {
        String creatingDate = new SimpleDateFormat("HH:mm dd.MM.yyyy").format(oneNews.getCreatingDate());
        return NewsDTO.NewsDTOBuilder.newNewsDTO()
                .id(oneNews.getId())
                .userId(oneNews.getUser().getId())
                .userName(oneNews.getUser().getEmail())
                .title(oneNews.getTitle())
                .content(oneNews.getContent())
                .creatingDate(creatingDate)
                .imageName(oneNews.getImageName())
                .originalImageName(oneNews.getOriginalImageName())
                .build();
    }
}
