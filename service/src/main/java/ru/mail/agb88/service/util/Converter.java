package ru.mail.agb88.service.util;

import ru.mail.agb88.repository.model.User;
import ru.mail.agb88.service.dto.UserDTO;

/**
 * Created by AlexBal.
 */
public class Converter {

    // TODO Инициализировать коллекции на уровне web. На веб передавать коллекции пустыми, а по запросу из jsp их инициализировать
    public static UserDTO userToUserDTO(User user) {
        UserDTO userDTO = null;
        if (user != null) {
            userDTO = UserDTO.Builder.newUserDTO()
                    .id(user.getId())
                    .email(user.getEmail())
                    .role(user.getRole())
                    .blocked(user.isLocked())
                    .news(user.getNews())
                    .itemsInBasket(user.getItemsInBasket())
                    .orders(user.getOrders())
                    .build();
            if (user.getAddress() != null) {
                userDTO.setCountry(user.getAddress().getCountry());
                userDTO.setCity(user.getAddress().getCity());
                userDTO.setStreet(user.getAddress().getStreet());
                userDTO.setHome(user.getAddress().getHome());
                userDTO.setApp(user.getAddress().getApp());
            }
        }
        return userDTO;
    }
}
