package ru.mail.agb88.service.dto;

import java.util.List;

/**
 * Created by AlexBal.
 */
public class NewsDTOWrapper {
    private List<NewsDTO> newsDTO;
    private Integer count;
    private Integer currentPage;
    private Integer pages;

    public NewsDTOWrapper(List<NewsDTO> newsDTO, Integer count, Integer currentPage, Integer pages) {
        this.newsDTO = newsDTO;
        this.count = count;
        this.currentPage = currentPage;
        this.pages = pages;
    }

    public List<NewsDTO> getNewsDTO() {
        return newsDTO;
    }

    public void setNewsDTO(List<NewsDTO> newsDTO) {
        this.newsDTO = newsDTO;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }
}
