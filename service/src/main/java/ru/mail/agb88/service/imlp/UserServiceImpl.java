package ru.mail.agb88.service.imlp;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.agb88.repository.UserDAO;
import ru.mail.agb88.repository.model.Address;
import ru.mail.agb88.repository.model.Role;
import ru.mail.agb88.repository.model.User;
import ru.mail.agb88.service.UserService;
import ru.mail.agb88.service.util.Util;
import ru.mail.agb88.service.dto.UserDTO;
import ru.mail.agb88.service.dto.UserPrincipal;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static ru.mail.agb88.service.util.Converter.userToUserDTO;

@Service
public class UserServiceImpl implements UserService {
    private Logger logger = Logger.getLogger(UserServiceImpl.class);

    private UserDAO userDAO;
    private MessageSource msg;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserDAO userDAO, MessageSource msg) {
        this.userDAO = userDAO;
        this.msg = msg;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    // Возвращает userDTO
    @Override
    @Transactional
    public UserDTO getUserDTO(Long id) {
        return userToUserDTO(userDAO.findById(id));
    }

    @Override
    @Transactional
    public UserDTO getUserDTObyEmail(String email) {
        return userToUserDTO(getUserByEmail(email));
    }

    @Override
    @Transactional
    public User getUserByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    //* Возвращает список userDTO */
    @Override
    @Transactional
    public List<UserDTO> getAllUsersDTO() {
        List<UserDTO> usersDTO = new ArrayList<>();
        for (User user : userDAO.findAll()) {
            usersDTO.add(userToUserDTO(user));
        }
        return usersDTO;
    }

    // Сохраняет userDTO
    @Override
    @Transactional
    public void saveUserDTO(UserDTO userDTO) {
        saveUserDTO(userDTO, false);
    }

    // Сохраняет userDTO и аутентифицирует
    @Override
    @Transactional
    public void saveUserDTO(UserDTO userDTO, boolean authenticate) {
        //TODO Проверять, нужно ли кодировать пароль
        userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        User user = userDTOtoUser(userDTO);
        userDAO.save(user);
        if (authenticate) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(Role.USER.name()));
            Authentication authentication = new UsernamePasswordAuthenticationToken(new UserPrincipal(user), userDTO.getPassword(), authorities);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }

    //* Удаляет userDTO */
    @Override
    @Transactional
    public void deleteUserDTO(UserDTO userDTO) {
        userDAO.delete(userDTOtoUser(userDTO));
    }

    // Меняет роль, возвращает сообщение о результате смены роли
    @Override
    @Transactional
    public String changeRole(Long userId, Role role) {
        User user = userDAO.findById(userId);
        if (user.getRole() == role) {
            return msg.getMessage("equals.roles", null, Locale.getDefault());
        }

        if (user.getRole() == Role.SUPER_ADMIN && userDAO.countSuperAdmin() == 1) {
            return msg.getMessage("last.superadmin", null, Locale.getDefault());
        }

        userDAO.changeStatus(userId, role);
        // TODO разлогинить пользователя через SessionRegistrу
        return msg.getMessage("changerole.success", null, Locale.getDefault());
    }

    // Блокирует/разблокирует пользователя
    @Override
    @Transactional
    public String lock(Long userId, boolean lock) {
        UserPrincipal user = Util.findLoggedInUser();
        if (user.getId().equals(userId)){
            return msg.getMessage("lock.yourself", null, Locale.getDefault());
        }
        if (lock){
            userDAO.lock(userId);
            return msg.getMessage("lock", null, Locale.getDefault());
        } else {
            userDAO.unLock(userId);
            return msg.getMessage("unlock", null, Locale.getDefault());
        }
    }

    // Удаляет пользователя
    @Override
    @Transactional
    public void delete(Long userId) {
        userDAO.delete(userId);
    }

    @Override
    public void changePassword(UserDTO userDTO) {
        // TODO изменение пароля у пользователя из админки
    }

    //* Копируем userDTO в user */
    private User userDTOtoUser(UserDTO userDTO) {
        User user = new User();
        if (userDTO != null) {
            user.setEmail(userDTO.getEmail());
            user.setPassword(userDTO.getPassword());
            user.setPhone(userDTO.getPhone());
            if (userDTO.getRole() == null) {
                user.setRole(Role.USER);
            }
            if (userDTO.isLocked()) {
                user.setLocked(true);
            }
            if (userDTO.hasAddress()) {
                Address address = new Address();
                address.setCountry(userDTO.getCountry());
                address.setCity(userDTO.getCity());
                address.setStreet(userDTO.getStreet());
                address.setHome(userDTO.getHome());
                address.setApp(userDTO.getApp());
                user.setAddress(address);
            }
        }
        return user;
    }
}
