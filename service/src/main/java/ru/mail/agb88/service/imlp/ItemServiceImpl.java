package ru.mail.agb88.service.imlp;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import ru.mail.agb88.repository.ItemDAO;
import ru.mail.agb88.repository.model.Item;
import ru.mail.agb88.service.ItemService;
import ru.mail.agb88.service.util.Util;
import ru.mail.agb88.service.dto.ItemDTO;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    private ItemDAO itemDAO;
    private Environment env;
    private Logger logger = Logger.getLogger(ItemServiceImpl.class);

    @Autowired
    public ItemServiceImpl(ItemDAO itemDAO, Environment env) {
        this.itemDAO = itemDAO;
        this.env = env;
    }

    // Вовзращает список всех товаров
    @Override
    @Transactional
    public List<ItemDTO> getAllItemsDTO() {
        List<ItemDTO> itemsDTO = new ArrayList<>();
        for (Item item : itemDAO.findAll()) {
            itemsDTO.add(itemToItemDTO(item));
        }
        return itemsDTO;
    }

    // Сохраняет новый товар
    @Override
    @Transactional
    public void saveItemDTO(ItemDTO itemDTO) throws IOException {
        String modifiedImageName = null;
        if (!itemDTO.getImage().isEmpty()) {
            modifiedImageName = Util.getModifiedImageName(itemDTO.getImage().getOriginalFilename());
            itemDTO.setOriginalImageName(itemDTO.getImage().getOriginalFilename());
            itemDTO.setImageName(modifiedImageName);
        }

        itemDAO.save(itemDTOtoItem(itemDTO));

        if (!itemDTO.getImage().isEmpty()) {
            File result = new File(env.getProperty("path_to_images") + "items/" + modifiedImageName);
            FileCopyUtils.copy(itemDTO.getImage().getBytes(), result);
        }
    }

    @Override
    @Transactional
    // Копирует товар и сохраняет
    public void copyItemDTO(ItemDTO itemDTO) throws IOException {
        itemDTO.setId(null);
        if (!itemDTO.getImage().isEmpty()) { // когда есть новая картинка
            saveItemDTO(itemDTO);
            return;
        }

        String oldImageName = itemDTO.getImageName();
        String originalImageName = itemDTO.getOriginalImageName();
        itemDTO.setImageName(null);
        String modifiedImageName = null;
        if (originalImageName != null) { // когда оставили старую картинку
            modifiedImageName = Util.getModifiedImageName(originalImageName);
            itemDTO.setImageName(modifiedImageName);
        }

        itemDAO.save(itemDTOtoItem(itemDTO));

        if (originalImageName != null) {
            File in = new File(env.getProperty("path_to_images") + "items/" + oldImageName);
            File result = new File(env.getProperty("path_to_images") + "items/" + modifiedImageName);
            FileCopyUtils.copy(in, result);
        }
    }

    // Обновляет товар
    @Override
    @Transactional
    public void updateItemDTO(ItemDTO itemDTO) throws IOException {
        String oldImageName = itemDTO.getImageName();
        if (!itemDTO.getImage().isEmpty()) { // когда есть новая картинка
            saveItemDTO(itemDTO);
            new File(env.getProperty("path_to_images") + "items/" + oldImageName).delete();
            return;
        }

        if (itemDTO.getOriginalImageName() == null) { // без картинки
            new File(env.getProperty("path_to_images") + "items/" + oldImageName).delete();
            itemDTO.setImageName(null);
        }

        itemDAO.save(itemDTOtoItem(itemDTO));
    }

    // Удаляет товар и его картинку
    @Override
    @Transactional
    public void deleteItemDTO(Long itemId, String imageName) {
        itemDAO.delete(itemId);
        File file = new File(env.getProperty("path_to_images") + "items/" + imageName);
        if (file.exists()) {
            try {
                file.delete();
            } catch (SecurityException e) {
                logger.debug("File not found or can't delete");
            }
        }
    }

    @Override
    @Transactional
    public ItemDTO getItemDTOById(Long id) {
        return itemToItemDTO(itemDAO.findById(id));
    }

    // Определяет место страницы для копирования/обновления товара
    @Override
    public String determineJspUrl(String action) {
        String jspUrl = "/admin/items/items";
        switch (action) {
            case "copy": {
                jspUrl = "/admin/items/copyform";
                break;
            }
            case "update": {
                jspUrl = "/admin/items/updateform";
                break;
            }
        }
        return jspUrl;
    }

    //* Копируем item в itemDTO */
    private ItemDTO itemToItemDTO(Item item) {
        BigDecimal price = new BigDecimal(item.getPrice()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
        return ItemDTO.Builder.newItemDTO()
                .id(item.getId())
                .title(item.getTitle())
                .price(price.toString())
                .invNumber(item.getInvNumber())
                .description(item.getDescription())
                .imageName(item.getImageName())
                .originalImageName(item.getOriginalImageName())
                .build();
    }

    //* Копируем itemDTO в item */
    private Item itemDTOtoItem(ItemDTO itemDTO) throws NumberFormatException {
        BigDecimal price = new BigDecimal(itemDTO.getPrice()).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP);
        Item item = new Item();
        item.setId(itemDTO.getId());
        item.setTitle(itemDTO.getTitle());
        item.setPrice(price.intValue());
        item.setInvNumber(itemDTO.getInvNumber());
        item.setDescription(itemDTO.getDescription());
        item.setImageName(itemDTO.getImageName());
        item.setOriginalImageName(itemDTO.getOriginalImageName());
        return item;
    }
}

