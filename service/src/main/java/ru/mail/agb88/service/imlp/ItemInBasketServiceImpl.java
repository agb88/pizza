package ru.mail.agb88.service.imlp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mail.agb88.repository.ItemInBasketDAO;
import ru.mail.agb88.repository.model.ItemInBasket;
import ru.mail.agb88.service.ItemInBasketService;
import ru.mail.agb88.service.util.Util;
import ru.mail.agb88.service.dto.ItemDTO;
import ru.mail.agb88.service.dto.ItemsDTOWrapper;
import ru.mail.agb88.service.dto.UserPrincipal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class ItemInBasketServiceImpl implements ItemInBasketService {

    private Logger logger = Logger.getLogger(ItemInBasketServiceImpl.class);
    private ItemInBasketDAO itemInBasketDAO;

    @Autowired
    public ItemInBasketServiceImpl(ItemInBasketDAO itemInBasketDAO) {
        this.itemInBasketDAO = itemInBasketDAO;
    }

    // Получает все товары для пользователя, считает общую стоимость и возвращает класс-обертку
    @Transactional
    public ItemsDTOWrapper findAllItemsDTOForUser() {
        UserPrincipal userPrincipal = Util.findLoggedInUser();
        List<ItemInBasket> itemsInBasket = itemInBasketDAO.findAllForUser(userPrincipal.getId());
        List<ItemDTO> itemsDTO = new ArrayList<>();
        ItemsDTOWrapper itemsDTOWrapper = new ItemsDTOWrapper();
        for (ItemInBasket itemInBasket : itemsInBasket) {
            itemsDTO.add(itemInBasketToItemDTO(itemInBasket, itemsDTOWrapper));
        }
        itemsDTOWrapper.setItemsDTO(itemsDTO);
        return itemsDTOWrapper;
    }

    // Получает товар из корзины для пользователя
    @Override
    @Transactional
    public ItemDTO getItemInBasket(Long itemId) {
        UserPrincipal userPrincipal = Util.findLoggedInUser();
        ItemInBasket itemInBasket = itemInBasketDAO.findByUserIdAndItemId(userPrincipal.getId(), itemId);
        if (itemInBasket == null) {
            return null;
        }
        return itemInBasketToItemDTO(itemInBasket, null);
    }

    // Изменяет количество товара у пользователя в корзине
    @Override
    @Transactional
    public void updateItemInBasket(ItemDTO itemDTO) {
        UserPrincipal userPrincipal = Util.findLoggedInUser();
        ItemInBasket itemInBasket = itemInBasketDAO.findByUserIdAndItemId(userPrincipal.getId(), itemDTO.getId());
        if (itemInBasket != null && itemInBasket.getQuantity() != itemDTO.getQuantity()) {
            itemInBasket.setQuantity(itemDTO.getQuantity());
            itemInBasketDAO.save(itemInBasket);
        }
    }

    // Удаляет товар из корзины у пользователя
    @Override
    @Transactional
    public void deleteItemInBasket(Long itemId) {
        UserPrincipal userPrincipal = Util.findLoggedInUser();
        ItemInBasket itemInBasket = itemInBasketDAO.findByUserIdAndItemId(userPrincipal.getId(), itemId);
        if (itemInBasket != null) {
            itemInBasketDAO.delete(itemInBasket);
        }
    }

    // Добавляет товар в корзину
    @Transactional
    public void addItemInBasket(Long itemId) {
        UserPrincipal userPrincipal = Util.findLoggedInUser();
        ItemInBasket itemInBasket = itemInBasketDAO.findByUserIdAndItemId(userPrincipal.getId(), itemId);
        if (itemInBasket != null) {
            itemInBasket.setQuantity(itemInBasket.getQuantity() + 1);
            itemInBasketDAO.save(itemInBasket);
        } else {
            itemInBasketDAO.save(userPrincipal.getId(), itemId);
        }
    }

    private ItemDTO itemInBasketToItemDTO(ItemInBasket itemInBasket, ItemsDTOWrapper itemsDTOWrapper) {
        BigDecimal price = new BigDecimal(itemInBasket.getItem().getPrice()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
        Integer quantity = itemInBasket.getQuantity();
        BigDecimal totalPrice = price.multiply(new BigDecimal(quantity));
        if (itemsDTOWrapper != null) {
            itemsDTOWrapper.countTotalPrice(totalPrice);
        }
        return ItemDTO.Builder.newItemDTO()
                .id(itemInBasket.getItem().getId())
                .title(itemInBasket.getItem().getTitle())
                .price(price.toString())
                .totalPrice(totalPrice.toString())
                .invNumber(itemInBasket.getItem().getInvNumber())
                .description(itemInBasket.getItem().getDescription())
                .imageName(itemInBasket.getItem().getImageName())
                .originalImageName(itemInBasket.getItem().getOriginalImageName())
                .quantity(quantity)
                .build();
    }
}
