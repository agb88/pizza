package ru.mail.agb88.service.dto;


import org.springframework.web.multipart.MultipartFile;
import ru.mail.agb88.repository.model.Status;

public class ItemDTO {

    private Long id;
    private String title;
    private String price;
    private String totalPrice;
    private String invNumber;
    private String description;
    private String imageName;
    private String originalImageName;
    private MultipartFile image;
    private Integer quantity;
    private Long orderId;
    private String creatingDate;
    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getInvNumber() {
        return invNumber;
    }

    public void setInvNumber(String invNumber) {
        this.invNumber = invNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public String getOriginalImageName() {
        return originalImageName;
    }

    public void setOriginalImageName(String originalImageName) {
        this.originalImageName = originalImageName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(String creatingDate) {
        this.creatingDate = creatingDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public static final class Builder {
        private Long id;
        private String title;
        private String price;
        private String totalPrice;
        private String invNumber;
        private String description;
        private String imageName;
        private String originalImageName;
        private MultipartFile image;
        private Integer quantity;
        private Long orderId;
        private String creatingDate;
        private Status status;

        private Builder() {
        }

        public static Builder newItemDTO() {
            return new Builder();
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder price(String price) {
            this.price = price;
            return this;
        }

        public Builder totalPrice(String totalPrice) {
            this.totalPrice = totalPrice;
            return this;
        }

        public Builder invNumber(String invNumber) {
            this.invNumber = invNumber;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder imageName(String imageName) {
            this.imageName = imageName;
            return this;
        }

        public Builder originalImageName(String originalImageName) {
            this.originalImageName = originalImageName;
            return this;
        }

        public Builder image(MultipartFile image) {
            this.image = image;
            return this;
        }

        public Builder quantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder orderId(Long orderId) {
            this.orderId = orderId;
            return this;
        }

        public Builder creatingDate(String creatingDate) {
            this.creatingDate = creatingDate;
            return this;
        }

        public Builder status(Status status) {
            this.status = status;
            return this;
        }

        public ItemDTO build() {
            ItemDTO itemDTO = new ItemDTO();
            itemDTO.description = this.description;
            itemDTO.price = this.price;
            itemDTO.totalPrice = this.totalPrice;
            itemDTO.invNumber = this.invNumber;
            itemDTO.title = this.title;
            itemDTO.imageName = this.imageName;
            itemDTO.image = this.image;
            itemDTO.originalImageName = this.originalImageName;
            itemDTO.id = this.id;
            itemDTO.quantity = this.quantity;
            itemDTO.orderId = this.orderId;
            itemDTO.creatingDate = this.creatingDate;
            itemDTO.status = this.status;
            return itemDTO;
        }
    }
}
