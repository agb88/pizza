package ru.mail.agb88.service;

import ru.mail.agb88.service.dto.NewsDTO;
import ru.mail.agb88.service.dto.NewsDTOWrapper;

import java.io.IOException;

/**
 * Created by AlexBal.
 */
public interface NewsService {
    NewsDTOWrapper getAllNews(Integer page);

    NewsDTO getNews(Long newsId);

    void saveNewsDTO(NewsDTO newsDTO) throws IOException;

    void updateNewsDTO(NewsDTO newsDTO) throws IOException;

    void deleteNewsDTO(Long newsId, String imageName) throws IOException;
}
