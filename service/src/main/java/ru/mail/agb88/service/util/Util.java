package ru.mail.agb88.service.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import ru.mail.agb88.repository.model.Role;
import ru.mail.agb88.repository.model.Status;
import ru.mail.agb88.service.dto.UserPrincipal;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by AlexBal.
 */
public class Util {

    // Модифицирует имя картинки для добавления в БД
    public static String getModifiedImageName(String originalImageName) {
        SimpleDateFormat format = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        return originalImageName.substring(0, originalImageName.indexOf(".")) + "_" + format.format(new Date()) + originalImageName.substring(originalImageName.indexOf("."));
    }

    // Возвращает залогиненного пользователя
    public static UserPrincipal findLoggedInUser() {
        UserPrincipal userPrincipal = null;
        Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (o instanceof UserDetails) {
            userPrincipal = ((UserPrincipal) o);
        }
        return userPrincipal;
    }

    public static List<Status> getStatusList() {
        List<Status> status = new ArrayList<>();
        Collections.addAll(status,Status.values());
        return status;
    }

    public static List<Role> getRolesList() {
        List<Role> roles = new ArrayList<>();
        Collections.addAll(roles, Role.values());
        return roles;
    }
}
