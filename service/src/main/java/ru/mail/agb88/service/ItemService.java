package ru.mail.agb88.service;


import ru.mail.agb88.service.dto.ItemDTO;

import java.io.IOException;
import java.util.List;

public interface ItemService {
    List<ItemDTO> getAllItemsDTO();
    void saveItemDTO(ItemDTO itemDTO) throws IOException;
    void copyItemDTO(ItemDTO itemDTO) throws IOException;
    void updateItemDTO(ItemDTO itemDTO)throws IOException;
    void deleteItemDTO(Long itemId, String imageName);
    ItemDTO getItemDTOById(Long itemId);
    String determineJspUrl(String action);
}
