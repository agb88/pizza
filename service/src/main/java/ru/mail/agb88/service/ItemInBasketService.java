package ru.mail.agb88.service;

import ru.mail.agb88.service.dto.ItemDTO;
import ru.mail.agb88.service.dto.ItemsDTOWrapper;


public interface ItemInBasketService {
    ItemsDTOWrapper findAllItemsDTOForUser();

    void addItemInBasket(Long itemId);

    ItemDTO getItemInBasket(Long itemId);

    void updateItemInBasket(ItemDTO itemDTO);

    void deleteItemInBasket(Long itemId);
}
